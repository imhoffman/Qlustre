# Qlustre Upgrade
The base system described on [the Qlustre page](https://gitlab.com/imhoffman/Qlustre) was expanded as described here.
Please be sure to read [the initial description](https://gitlab.com/imhoffman/Qlustre) in order to understand this page.

After measuring the power draw of a single node during parallel computation, I realized that more than five nodes could be powered by a typical commercial hub PSU.
After a little shopping for the most ports on a network switch that I could power with 5 Volts, I decided on [this Amcrest 8-port network switch](https://www.amazon.com/Amcrest-8-Port-Gigabit-Ethernet-Network/dp/B077V3GJHQ/) and [this Aniker 10-port PSU](https://www.amazon.com/Anker-10-Port-Charger-PowerPort-iPhone/dp/B00YRYS4T4/).

# construction
## additional parts
I bought three more Pi 3 B+, three more iUniker cases, and three more 64-GB SD cards in order to completely duplicate the existing nodes.
Since the 10-port Aniker does not come with cables like the 6-port does, I ordered [some](https://www.amazon.com/gp/product/B01LZ6458B/) [angled](https://www.amazon.com/gp/product/B072FH4JBL/) [cables](https://www.amazon.com/gp/product/B01N337FQF/) to support the stack design that I had in mind.
One great feature of the 10-port Aniker is that it has a power switch on the unit&mdash;so, I can bring the whole cluster up or down with that single switch.
The PSU is still only 60 W, but I am nevertheless able to power eight Pis (including two small case fans), the network switch, and [a USB cooling fan](https://www.amazon.com/gp/product/B00G05A2MU/).
The same USB power cable that I was using for the Netgear switch also fits the Amcrest.
I also bought three more 1-ft Cat6A cables.

## the stack
I decided not to employ a single stack like the five Pis on the original cluster for two reasons:
1. The stack would be too tall for 1-ft network cables to reach throughout unless it were laid on its side, which I consider unseemly.
2. I want a single, large fan to be able to vent the open cases through their sides.

So, I arranged the eight Pis into two four-layer stacks of the same iUniker cases.

[![twin](../images/IMG_0437_small.JPG)](../images/IMG_0437.JPG) ![towers](../images/IMG_0430_small.JPG)

I happened to have two [four-layer dog bone cases](https://www.amazon.com/gp/product/B01LVUVVOQ/) around, which I raided for parts in order to tie those two stacks together.
It turns out that the short brass posts in the dog bone cases have the same thread depth as the tall brass posts of the iUniker cases.
So, I was able to terminate four of the eight tall brass posts in the twin stacks with the short spacers.
These short spacers could then, in turn, be finished with some of the extra iUniker top planes that I had left over from building the stacks.
Lastly, the planes are finished with nuts (on the top) or screws (on the bottom) in order to bridge the two stacks at their top and bottom.

![nuts](../images/IMG_0433_small.JPG) ![bolts](../images/IMG_0438_small.JPG)

The height and width of the gap between the stacks is ***perfect*** for the large cooling fan!

When I took these pictures, I did not yet have angled USB micro-B power cords, so the fan cannot yet be inserted all the way.
Also, in these photos, the currents shown on the in-line USB meters are for hal03 and hal07 while idling.

[![pretty](../images/IMG_0435_small.JPG)](../images/IMG_0435.JPG)

# configuration
## new nodes
Rather than build the software on each new node from source, I cloned one of the 64-GB SD cards from one of the original nodes.
(I chose to clone hal02 because it was the only node to achieve a perfect score on the OpenMPI make tests.)
I cloned the cards using a laptop with an SD card reader that was running ubuntu 16.04.
As per [the instructions](https://www.raspberrypi.org/documentation/linux/filesystem/backup.md), I used `dd` on the unmounted card

    $ sudo dd bs=4M if=/dev/mmcblk0 | gzip > hal02.img.gz
and then after inserting a new SD card of the same size right out of the package,

    $ sudo gunzip --stdout hal02.img.gz | sudo dd bs=4M of=/dev/mmcblk0
With three SD cards written and inserted into the new Pis, I plugged my monitor and keyboard/mouse into each new node and booted it.
Because they are clones of hal02, they boot to the CLI and think that they are hal02.
Thus, the obvious changes need to be made to the following files:
1. `/etc/hostname`
1. `/etc/hosts`

and you must
1. note the hardware address of the ethernet adapter
1. delete the existing `~/.ssh/id_rsa` and `~/.ssh/id_rsa.pub`
1. and I deleted the existing `~/.ssh/authorized_keys` and `~/.ssh/known_hosts`

Then, reboot and generate a new key for each host.

    $ ssh-keygen
hitting enter for the default location and lack of password.
As before, with such a small number of nodes, I then chose to manually log in to each node in order to distribute the new keys using `ssh-copy-id` as before.

## IP assignments

1. Add the hardware addresses of the new nodes, along with whatever IP addresses you'd like them to have, to `/etc/dnsmasq.conf`.
1. Add all of the new nodes to `/etc/hosts` on the head node as well as on the original nodes.
1. Add the new nodes to `/etc/exports` on the head node (if using nfs).

## MPI configuration

Add the new hosts to the `machinefile` that you pass to the MPI.

## I2C

Since my need is to run this system headless by walking into a classroom and simply plugging in the power, I need to know the IP at which to `ssh` in.
I have already configured the wifi to establish the connection, I simply need to know the IP that is assigned from DHCP.
While I could keep the wlan hardware address written down and `arpscan` for it, I wanted something cooler!
So, I bought a little [I2C OLED display](https://www.amazon.com/gp/product/B00O2LLT30/) and wired it in for 3V3.

![oled](../images/IMG_0443_small.JPG)

I wrote [this python script](system/i2c.py) with help from [here](http://codelectron.com/setup-oled-display-raspberry-pi-python/) and [here](http://code.activestate.com/recipes/439094-get-the-ip-address-associated-with-a-network-inter/).
It starts at boot with the following line in `rc.local`

    /usr/bin/python3 /home/pi/Documents/i2c.py &
and can be configured and/or shutdown afterward through two files that it reads in a loop.
Basically, it waits 45 seconds initially for DHCP to settle with the assigned IP address, then loops every 10 seconds thereafter looking for changes to the IP address or the two control files.

## secure the user "pi" and establish an account for cluster computing

Since this cluster will be used by students and other supported users, the cluster must be configured for login and MPI use by users other than `pi`.
Furthermore, these guests should only have access to computing resources and not to administration (such as the plain-text credentials in `wpa_supplicant.conf`).

First, each node should be configured to boot to a command-line login, not automatically as `pi`.
(I should have done this initially, but didn't get to it until now.)
This can be done by `ssh`ing to each node and running

    $ sudo raspi-config
from the command line.
In the menus, choose `3 Boot Options` then `B1 Desktop / CLI` then `B1 Console`, then `<Finish>` and reboot&mdash;but wait!
Also, since we are not planning on using any graphics on the computing nodes, we should minimize the memory that is split out for the GPU.
This is also done in `raspi-config` in `7 Advanced Options` then `A3 Memory Split` then type in `16`, then `<Finish>` and reboot.

Then, create a user on the head node and via `ssh` to each node.
First, add the PATH to MPI, as [initially](https://gitlab.com/imhoffman/Qlustre), to the `.bashrc` in `/etc/skel` then

    $ sudo adduser quest
in order to add a user named `quest`.
The new user will be given their own group.
In order to keep the user out of the `pi` files, you may also issue

    $ sudo chmod go-rwx /home/pi
while you are there.
Then, `ssh` into each node as the new user and create an ssh key for them.

    $ ssh-keygen
and then distribute the keys via your chosen method, as [initially](https://gitlab.com/imhoffman/Qlustre).
Since the new user won't be able to read the source directories of MPICH and mpi4py, I recommend copying all of the example code from those directories to the new user's `home`.

# performance
## power
I've got [an in-line power meter](https://www.amazon.com/gp/product/B0777H8MS8/) on the AC line into the Aniker PSU.
With the cluster running a computation on all nodes and powering two small case fans and the large cooling fan (but not the network switch), the consumption is nearly 48 W.

![power](../images/IMG_0440_small.JPG)

When the system is idling, the consumption is approximately 24 W.

## temperatures
I wrote [this script](system/temps.sh) to format the CPU and GPU temperatures for display and [this script](system/temperatures.sh) to collect the information from the nodes and display it all.

```
pi@hal:~ $ /mnt/nfs/shared/bin/temperatures.sh 

hal   --  GPU: 29.5 C   CPU: 28.9 C
hal01 --  GPU: 31.1 C   CPU: 31.0 C
hal02 --  GPU: 28.9 C   CPU: 28.9 C
hal03 --  GPU: 26.8 C   CPU: 26.7 C
hal04 --  GPU: 28.4 C   CPU: 27.8 C
hal05 --  GPU: 29.5 C   CPU: 30.0 C
hal06 --  GPU: 28.9 C   CPU: 28.9 C
hal07 --  GPU: 26.8 C   CPU: 26.7 C
```
Note that I keep these executables in `/mnt/nfs/shared/bin` so that all the nodes can see them.
I also have `/mnt/nfs/shared/tmp` for scratch files from all users.

## MPICH versus OpenMPI benchmarking
I modified [`monte_carlo_mpi.c`](http://people.sc.fsu.edu/~jburkardt/c_src/mpi/monte_carlo_mpi.c) to have a tolerance of `1E-08` and to not print every return to the terminal.
When compiled and run with MPICH, I get

    pi@hal:/mnt/nfs/shared/mpi $ /home/rpimpi/mpi-install/bin/mpicc ~/Documents/monte_carlo_mpi.c
    pi@hal:/mnt/nfs/shared/mpi $ /home/rpimpi/mpi-install/bin/mpiexec -f ~/hostfile -n 28 ./a.out
    ...
    pi =  3.14159265124547149028
    
    points: 221643000
    in: 174078005, out: 47564995
    
      Elapsed wallclock time = 73.1202 seconds.

However, when compiled and run with OpenMPI, I get

    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpicc ~/Documents/monte_carlo_mpi.c 
    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpiexec -machinefile ~/nodefile -n 28 ./a.out 
    ...

and it doesn't finish! (at least for 15 minutes, or so.)
The link lights were much less active on the network switch, too.
OpenMPI is able to handle Hello World and it works for ring tests in [C](http://people.sc.fsu.edu/~jburkardt/c_src/ring_mpi/ring_mpi.c) and [fortran](http://people.sc.fsu.edu/~jburkardt/f_src/ring_mpi/ring_mpi.f90):

```
pi@hal:/mnt/nfs/shared/mpi $ /home/rpimpi/mpi-install/bin/mpicc ~/Documents/ring_mpi.c 
pi@hal:/mnt/nfs/shared/mpi $ /home/rpimpi/mpi-install/bin/mpiexec -f ~/hostfile -n 28 ./a.out 

RING_MPI:
  C/MPI version
  Measure time required to transmit data around
  a ring of processes

  The number of processes is 28

  Timings based on 10 experiments
  N double precision values were sent
  in a ring transmission starting and ending at process 0
  and using a total of 28 processes.

         N           T min           T ave           T max

       100      0.00146961      0.00305846       0.0158994
      1000      0.00457501      0.00482073      0.00579929
     10000       0.0246704       0.0265087       0.0346344
    100000        0.219845        0.220457        0.223138
   1000000          2.0671         2.08128         2.18911

RING_MPI:
  Normal end of execution.
pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpicc ~/Documents/ring_mpi.c 
pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpiexec -machinefile ~/nodefile -n 28 ./a.out
RING_MPI:
  C/MPI version
  Measure time required to transmit data around
  a ring of processes

  The number of processes is 28

  Timings based on 10 experiments
  N double precision values were sent
  in a ring transmission starting and ending at process 0
  and using a total of 28 processes.

         N           T min           T ave           T max

       100       0.0012701       0.0128634        0.116385
      1000      0.00398936      0.00444713      0.00617945
     10000       0.0245682       0.0254082       0.0282286
    100000        0.211216        0.212818        0.222467
   1000000         2.06415          2.0764         2.16587

RING_MPI:
  Normal end of execution

pi@hal:/mnt/nfs/shared/mpi $ /home/rpimpi/mpi-install/bin/mpif90 ~/Documents/ring_mpi.f90 
pi@hal:/mnt/nfs/shared/mpi $ /home/rpimpi/mpi-install/bin/mpiexec -f ~/hostfile -n 28 ./a.out

...
 
RING_MPI:
  FORTRAN90/MPI version
  Measure time required to transmit data around
  a ring of processes
 
  The number of processes is       28
 
  Timings based on     10 experiments
  N double precision values were sent
  in a ring transmission starting and ending at process 0
  and using a total of     28 processes.
 
         N           T min           T ave           T max
 
       100    0.125813E-02    0.233276E-02    0.114474E-01
      1000    0.384712E-02    0.401123E-02    0.458264E-02
     10000    0.244408E-01    0.265924E-01    0.307593E-01
    100000    0.220006        0.220681        0.223660    
   1000000     2.07143         2.08556         2.19279    
 
RING_MPI:
  Normal end of execution.
 
pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpif90 ~/Documents/ring_mpi.f90 
pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpiexec -machinefile ~/nodefile ./a.out

...
 
RING_MPI:
  FORTRAN90/MPI version
  Measure time required to transmit data around
  a ring of processes
 
  The number of processes is       28
 
  Timings based on     10 experiments
  N double precision values were sent
  in a ring transmission starting and ending at process 0
  and using a total of     28 processes.
 
         N           T min           T ave           T max
 
       100    0.126978E-02    0.165008E-01    0.152461    
      1000    0.402845E-02    0.460946E-02    0.675556E-02
     10000    0.248679E-01    0.255415E-01    0.288776E-01
    100000    0.211429        0.212630        0.221884    
   1000000     2.06347         2.14585         2.77039    
 
RING_MPI:
  Normal end of execution.
```
so I'm not sure what's going on with the Monte Carlo in OpenMPI.
