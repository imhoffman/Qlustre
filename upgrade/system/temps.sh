#!/bin/bash
# the temperatures.sh script expects the following value for $log
log="/mnt/nfs/shared/tmp/temperatures"
host=$(hostname)
if [ ${#host} -lt 5 ]
then
	host=$(hostname)"  "
fi
cpuk=$(</sys/class/thermal/thermal_zone0/temp)
cpu=$(bc <<< "scale=1; $cpuk/1000")
gpu=$(vcgencmd measure_temp)
echo "$host --  GPU: ${gpu:5:4} C   CPU: $cpu C" >> $log
