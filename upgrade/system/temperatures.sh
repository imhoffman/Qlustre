#!/bin/bash
log="/mnt/nfs/shared/tmp/temperatures"
if [ -e $log ]
then
	rm -f $log
fi
echo "" >> $log
/mnt/nfs/shared/bin/temps.sh
declare -a nodes=("01" "02" "03" "04" "05" "06" "07")
for s in "${nodes[@]}"
	do ssh pi@hal"$s" "/mnt/nfs/shared/bin/temps.sh"
done
echo "" >> $log
cat $log
