# I2C imports
# I2C code from http://codelectron.com/setup-oled-display-raspberry-pi-python/
from luma.core.interface.serial import i2c
from luma.core.render import canvas
from luma.oled.device import ssd1306, ssd1325, ssd1331, sh1106
import time
from PIL import ImageFont, ImageDraw
# IP imports
# IP code from http://code.activestate.com/recipes/439094-get-the-ip-address-associated-with-a-network-inter/
import socket
import fcntl
import struct

# this will be run at boot, so allow time for the network to settle
time.sleep(45);

# setup I2C comm
serial = i2c(port=1, address=0x3C)
device = ssd1306(serial, rotate=0)

# loop while runfile reads "1"
switch = "1";
while ( switch == "1" ):
 def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', bytes(ifname[:15],'utf-8')) # bytes for Python3
    )[20:24])

 eth0 = get_ip_address('eth0')
 wlan0 = get_ip_address('wlan0')
 # end of IP code

 # place displayed text in a file that is periodically read
 with open('/home/pi/I2Cmessage.txt','r') as messagefile:
    words = messagefile.read().replace('\n','');

 # write to display
 with canvas(device) as draw:
    font1 = ImageFont.truetype('/opt/Wolfram/WolframEngine/11.2/SystemFiles/Fonts/TrueType/VeraMono-Bold.ttf',16)
    font2 = ImageFont.truetype('/usr/share/fonts/truetype/liberation2/LiberationSerif-Italic.ttf',40)
    draw.text((0, 0), wlan0, font=font1, fill="white")
    draw.text((4, 18), words, font=font2, fill="white")

 # loop delay
 time.sleep(10)

 # check that runfile still reads "1"
 with open('/home/pi/I2Crun.txt','r') as runfile:
    switch = runfile.read().replace('\n','');

# end of main loop, exited because runfile did not read "1"

# cleanup the files
runfile.close();
messagefile.close();

# set the runfile back to "1" for next boot
# because I know I'll forget and remember once it's headless
with open('/home/pi/I2Crun.txt','w') as closefile:
    closefile.write("1\n");
    closefile.close();
