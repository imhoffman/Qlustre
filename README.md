# Qlustre
The [Quest Liberal-arts University](http://questu.ca) Supercomputer for Teaching, Research, and Exploration.
The name is nothing more than the obvious joke about British spelling and a "Q" for Quest&mdash;there is nothing particularly lustrous about it.
We pronounce it "cluster," not "q-lustre."

This system has since been upgraded.
After you read this page, follow the latest developments on [the upgrade page](upgrade) and on [the examples page](examples).

This README is a description of the construction, commissioning, and ongoing use of a parallel-computing cluster of Raspberry Pis.
I was inspired mainly by these two descriptions ([here](https://makezine.com/projects/build-a-compact-4-node-raspberry-pi-cluster/) and [here](http://thundaxsoftware.blogspot.ca/2016/07/creating-raspberry-pi-3-cluster.html)) of existing clusters and I give them much of the credit for making this happen.
I am also grateful for being able to apply my professional development funding toward this equipment as part of my teaching of introductory Computer Programming.
Please drop me a line if you find this description useful (or if you find an error!).

# construction
## parts list
* 5 x Raspberry Pi Model 3 B+
* 4 x 64 GB U3 micro SD card
* 1 x 128 GB U3 micro SD card
* 5 x [iUniker fan case](https://www.amazon.com/gp/product/B01LXSMY1N/)
* [5-color heat sink pack](https://www.amazon.com/gp/product/B01LXSMY1N/)
* [5-pack 1-ft Cat6a cables](https://www.amazon.com/gp/product/B01LXSMY1N/)
* [Netgear GS305 5-port gigabit ethernet switch](https://www.amazon.com/gp/product/B01LXSMY1N/)
* [Aniker 60 W 6-jack USB power hub w/ six 1-ft B-to-micro cables](https://www.amazon.com/gp/product/B01LXSMY1N/)
* [1-ft USB/B-to-5.5mm/2.5mm/barrel power cable](https://www.amazon.ca/gp/product/B0772VD1JF/)

Of course, for configuring the system, you'll also need a keyboard/mouse, HDMI monitor, and a separate system on which to download and flash the SD cards.

## the parts
The ethernet switch was chosen because it is 5-Volt and therefore can be powered by a USB hub.
The size of the 64-GB cards on the computing nodes are probably overkill, so feel free to go as small as you can on those.
The 128-GB card for the head node is intentionally large, as I'll describe later.
The rainbow heat sinks are purely vain excess&mdash;the cases each come with a nice pair of non-tinted sinks, but I wanted some bling for $8.

Incidentally, while clusters in general can scale *ad infinitum*, the number of nodes is practically limited by hardware considerations.
In this case, wanting to power the whole system (including the network switch) with a single USB PSU sets the size at five Pis.
So, I went with five Pis rather than eight&mdash;just so that when this system is running headless on WiFi, only a single cable (the power) runs from the cluster to the wall.

## assembly
I was sold on the multi-color heat sinks the moment that I saw them.
Despite the comments on the amazon site, the sinks have the proper tape.
Here is a picture of the four computing nodes in their fancy clothes and the head node in its solo case before I built the rest of the stack.
(For all of these images, full-resolution versions are available in the [images](images) directory.)

![all five](images/IMG_0406_small.JPG)

I like the iUniker fan cases.
The fans are quiet, the fan guards are robust, and the parts have always been good (only once in six cases did I get one stripped screw; but they include spares).
I like these cases for single Pis and&mdash;although they are not marketed as stackable&mdash;I found that they will indeed stack.
I decided against the dog bones because they have a larger footprint (the iUniker case has the same footprint as the 6-port Aniker) and because they only come in a four pack (and I have five Pis).
I only used some of each case kit; the only plane with a fan is the top of the stack, so I've got a box of leftover unused fans and top planes.
Here is an image of the bottom two nodes mounted to their bottom planes and ready to be stacked.

![bottom of](images/IMG_0410_small.JPG) ![the stack](images/IMG_0411_small.JPG)

The rest goes together pretty obviously, with only the top of the stack being finished with a top/fan plane and nuts.
The top unit will serve as my head node and in the picture can already be seen the USB dongle for my beloved Logitech K400+ keyboard/mouse combo.

![the stacked case](images/IMG_0413_small.JPG) ![the whole thing](images/IMG_0415_small.JPG)

On the right can be seen the Aniker PSU and the Netgear switch, for completeness.

I bought shielded Cat6A cables because I was not sure if crosstalk throughout the cluster would be a problem.
They are a little stiffer and the turning radius of my 1-ft cables limits the physical layout of the stack a bit&mdash;be sure to consider cable length and stack construction when you are shopping.

# configuration

1. With a Windows 10 laptop that has an SD slot, I downloaded (and verified the sha256sum of!) the [zipped image of v4.14 (2018-04-18) of Raspbian Stretch desktop](https://www.raspberrypi.org/downloads/raspbian/).
After formatting with [`SD Formatter`](https://www.sdcard.org/downloads/formatter_4/), I used [`etcher`](https://etcher.io/) to flash the unzipped images to the four 64-GB cards and the one 128-GB card, as per [the instructions](https://www.raspberrypi.org/documentation/installation/installing-images/README.md).
2. The 128-GB card was booted on the head node, meaning that Raspbian automatically resized its root partition to extend over the entire card, then rebooted.
However, I did not want a single, monolithic partition on the head node.
So, once the automatic resize-and-reboot was finished, I powered down and removed the SD card.
On a different laptop with an SD slot running Ubuntu 16.04, I tried to repartition the card.
Unfortunately, the current Ubuntu port of `gparted` cannot handle the latest SD formats.
So, I built the latest `e2fsck` and `gparted` from source, altered the Raspbian card to have only a 51-GB root partition and added a new 67-GB ext4 partition for `nfs` use later&mdash;these sizes are somewhat arbitrary.
I did not alter the 64-GB cards at all from the initial flash.
3. Next, each node is plugged into the monitor and keyboard/mouse and booted for their initial Raspbian configuration.
In the case of the head node, this was not its first boot&mdash;for the computing nodes, they first automatically resized their SD partitions, then rebooted into user mode.
In all cases, X came up and I ran the GUI raspi-config from the desktop menu.
In all cases, I set the locale, timezone, password, hostname, etc.
On the head node, I selected SSH and VNC to be allowed&mdash;SSH is essential; VNC is for my own planned future headless access.
On the computing nodes, I selected only SSH and I selected boot to CLI.
Since hitting OK in the GUI will send you to reboot, also do the following in a terminal before departing.
 1. I'll be connecting to the nodes via ethernet, so I turn off BlueTooth and WiFi on the computing nodes (in order to conserve power and to reduce interference) by adding the following lines to the end of `/boot/config.txt`

      ```
      dtoverlay=pi3-disable-wifi
      dtoverlay=pi3-disable-bt
      ```
 2. Note the MAC hardware address (perhaps from `ifconfig`) of each ethernet adapter for later assignment.
I tediously went through and did the exact same GUI config (and the builds described shortly) on all of the computing nodes individually.
You might consider doing this only once and then cloning that SD card for the other nodes, like [Jordi Corbilla](http://thundaxsoftware.blogspot.ca/2016/07/creating-raspberry-pi-3-cluster.html) did.
Certainly if I add any more nodes, I'll be going the cloning route.
(Of course, even for cloned cards, you'll need to set the hostname and learn the unique MAC address from the Pi in which that card ultimately lives.)
4. Next, I set up the internet connection and cluster-only LAN.
My plan is for the head node to connect to my university WiFi and for it to share its internet connection over the ethernet LAN to the rest of the cluster.
(Access to the larger world by the computing nodes is not strictly needed.
I plan to use an `nfs` share for the `apt` cache and I could set up the head node as a time server, etc.
Nevertheless, it is handy to have Internet access for all nodes.)
In my case, I edited by `wpa_supplicant.conf` for my PEAP network and then used the guidance for Raspbian Stretch at [this link](https://raspberrypi.stackexchange.com/questions/48307/sharing-the-pis-wifi-connection-through-the-ethernet-port) to forward via `dnsmasq`, making my own [`wifi-to-eth-route.sh`](system/wifi-to-eth-route.sh) script that I run from  `rc.local` with the following line

     ```
     sudo /bin/bash /home/pi/wifi-to-eth-route.sh
     ```
(not from `/home/pi/.config/lxsession/LXDE-pi/autostart`, as suggested).
In `/etc/dnsmasq.conf` I added the following lines for my four nodes,

     ```
     dhcp-host=hal01,b8:27:eb:XX:XX:XX,192.168.2.11,infinite
     dhcp-host=hal02,b8:27:eb:XX:XX:XX,192.168.2.12,infinite
     dhcp-host=hal03,b8:27:eb:XX:XX:XX,192.168.2.13,infinite
     dhcp-host=hal04,b8:27:eb:XX:XX:XX,192.168.2.14,infinite
     ```
where the `XX:XX:XX` are the hardware addresses that I learned when initially configuring Raspbian on each node.
In this way, I always know the IP addresses of my nodes by name&mdash;this information can then be put in `/etc/hosts`, used for the `hostfile` for MPI, and used for `nfs`.
On my system, the head node is named hal and the computing nodes are hal01, hal02, etc.
At this point, you should be able to have all the cards in their respective Pis and have all of the Pis connected via ethernet cables to the switch.
Booting them all in this configuration should automatically configure the network and forwarding such that commands like

    ```
    pi@hal:~ $ ssh pi@hal01
    pi@hal01:~ $ sudo apt-get update
    ```
should yield an update on hal01 as hal01 accesses the internet via hal.
1. Note that in the preceding step, no password was required for the ssh login.
That's because I'd already done this step: generating and copying the ssh keys around.
 1. On each system, you'll need to generate a key using

       ```
       $ ssh-keygen
       ```
 and hit enter when asked for a location or for a key password.
 2. Then, distribute these keys.
In my case, I again did this manually on every system by ssh'ing to that system and executing, for example,

      ```
      $ ssh-copy-id pi@hal02
      ```
and giving the password, and then permuting this effort of mine around the cluster.
Alternatively, the tutorial from [Jordi Corbilla](http://thundaxsoftware.blogspot.ca/2016/07/creating-raspberry-pi-3-cluster.html) linked from the top of this page has a scripted method for collecting and distributing keys&mdash;do it that way, if you like; it is more elegant.
Regardless, this step is a one-time effort to get everybody to know each other.
1. &Agrave; la [Alasdair Allan](https://makezine.com/projects/build-a-compact-4-node-raspberry-pi-cluster/), I configured `nfs` and `autofs`.
In my case, the nfs drive is not an external USB flash dongle but rather is the additional partition that I created on the 128-GB SD card in the head node.
You'll need to install `nfs-server` on the head node

    ```
    pi@hal:~ $ sudo apt-get install nfs-server
    ```
and `nfs-common` for the client computing nodes

    ```
    pi@hal04:~ $ sudo apt-get install nfs-common
    ```
The mount point of the partition on the head node is `/mnt/nfs/shared` which was created by

    ```
    pi@hal:~ $ sudo mkdir -p /mnt/nfs/shared
    pi@hal:~ $ sudo chown -R pi:pi /mnt/nfs/shared
    ```
This is then set up to be shared in `/etc/exports` by adding

    ```
    /mnt/nfs/shared hal01(rw,sync,no_root_squash)
    /mnt/nfs/shared hal02(rw,sync,no_root_squash)
    /mnt/nfs/shared hal03(rw,sync,no_root_squash)
    /mnt/nfs/shared hal04(rw,sync,no_root_squash)
    ```
(The need for `no_root_squash` is not for MPI, but rather for `apt` and other commands I'd like to run remotely via `sudo`&mdash;see the section later concerning `nfs`.)
On each computing node, `autofs` must be installed

    ```
    pi@hal03:~ $ sudo apt-get install autofs
    ```
and then add the following line to `/etc/auto.master` 

    ```
    /mnt/nfs /etc/auto.nfs
    ```
and then create the file `/etc/auto.nfs` to include the following single line

    ```
    shared hal:/mnt/nfs/shared
    ```
If you plan to run your executables from the nfs share, *it is essential* that when you add this new partition to `/etc/fstab` that it ends up with the `exec` option.
That is, its line in my [`/etc/fstab`](system/fstab) on my head node is

    ```
    PARTUUID=b5cc0203-03  /mnt/nfs/shared ext4 defaults,user,exec  0       1
    ```
and `exec` is listed ***after*** `user` so that the `noexec` option of `user` is overridden.
Otherwise, the head node won't have permission to run its own binaries.
After everybody reboots, here is how the mounted partitions look on my head node:

    ```
    pi@hal:~ $ df -Th
    Filesystem     Type      Size  Used Avail Use% Mounted on
    /dev/root      ext4       51G  5.6G   43G  12% /
    devtmpfs       devtmpfs  460M     0  460M   0% /dev
    tmpfs          tmpfs     464M   16M  448M   4% /dev/shm
    tmpfs          tmpfs     464M   13M  452M   3% /run
    tmpfs          tmpfs     5.0M  4.0K  5.0M   1% /run/lock
    tmpfs          tmpfs     464M     0  464M   0% /sys/fs/cgroup
    /dev/mmcblk0p1 vfat       43M   22M   21M  51% /boot
    /dev/mmcblk0p3 ext4       67G  3.1G   60G   5% /mnt/nfs/shared
    tmpfs          tmpfs      93M     0   93M   0% /run/user/1000
    ```
and here is how they look on one of the computing nodes

    ```
    pi@hal02:~ $ df -Th
    Filesystem          Type      Size  Used Avail Use% Mounted on
    /dev/root           ext4       59G  4.7G   52G   9% /
    devtmpfs            devtmpfs  460M     0  460M   0% /dev
    tmpfs               tmpfs     464M     0  464M   0% /dev/shm
    tmpfs               tmpfs     464M   18M  446M   4% /run
    tmpfs               tmpfs     5.0M  4.0K  5.0M   1% /run/lock
    tmpfs               tmpfs     464M     0  464M   0% /sys/fs/cgroup
    /dev/mmcblk0p1      vfat       43M   22M   21M  51% /boot
    tmpfs               tmpfs      93M     0   93M   0% /run/user/1000
    hal:/mnt/nfs/shared nfs4       67G  3.1G   60G   5% /mnt/nfs/shared
    ```
Beware that on the client nodes, the nfs directory only appears one it is accessed&mdash;that is, not automatically at boot time.
So, `touch`ing it will work, or simply use it.
1. Next, MPI must be installed on the cluster.
I use [MPICH](http://www.mpich.org) and build it from source.
(The current version for me was 3.2.1.)
From the MPICH documentation, you'll need:

    ```
    - REQUIRED: A C compiler (gcc is sufficient)

    - OPTIONAL: A C++ compiler, if C++ applications are to be used
      (g++, etc.). If you do not require support for C++ applications,
      you can disable this support using the configure option
      --disable-cxx (configuring MPICH is described in step 1(d)
      below).

    - OPTIONAL: A Fortran compiler, if Fortran applications are to be
      used (gfortran, ifort, etc.). If you do not require support for
      Fortran applications, you can disable this support using
      --disable-fortran (configuring MPICH is described in step 1(d)
      below).
    ```
I plan on using my cluster for teaching a variety of languages, so I want it all!
(I'll also use python, for which a separate interface to the C libraries will be built later.)
Raspbian should already have C and C++ and so I only need to install fortran

    ```
    $ sudo apt-get install gfortran
    ```
With gfortran installed, the `configure` script will detect it and include fortran support.
With my source tarball from mpich.org unpacked in `/usr/local/src` and in the working directory `~/mpi-build`, the build is a typical one and I used a similar directory setup as my guiding tutorial:

    ```
    $ sudo mkdir -p /home/rpimpi/mpi-install
    $ sudo /usr/local/src/mpich-3.2.1/configure --prefix=/home/rpimpi/mpi-install
    $ sudo make && sudo make check
    $ sudo make install
    ```
The `check` runs a dozen tests or so, which is always comforting.
Once installed, add `/home/rpimpi/mpi-install/bin` to your `PATH`.
**Do this same build and install** on all of the computing nodes.
The installed programs

    ```
    pi@hal:~ $ ls -l /home/rpimpi/mpi-install/bin/
    total 7000
    -rwxr-xr-x 1 root root 1259440 May 10 22:30 hydra_nameserver
    -rwxr-xr-x 1 root root 1256772 May 10 22:30 hydra_persist
    -rwxr-xr-x 1 root root 1477764 May 10 22:30 hydra_pmi_proxy
    lrwxrwxrwx 1 root root       6 May 10 22:31 mpic++ -> mpicxx
    -rwxr-xr-x 1 root root    9511 May 10 22:31 mpicc
    -rwxr-xr-x 1 root root    8724 May 10 22:30 mpichversion
    -rwxr-xr-x 1 root root    9086 May 10 22:31 mpicxx
    lrwxrwxrwx 1 root root      13 May 10 22:30 mpiexec -> mpiexec.hydra
    -rwxr-xr-x 1 root root 1741064 May 10 22:30 mpiexec.hydra
    lrwxrwxrwx 1 root root       7 May 10 22:31 mpif77 -> mpifort
    lrwxrwxrwx 1 root root       7 May 10 22:31 mpif90 -> mpifort
    -rwxr-xr-x 1 root root   12762 May 10 22:31 mpifort
    lrwxrwxrwx 1 root root      13 May 10 22:30 mpirun -> mpiexec.hydra
    -rwxr-xr-x 1 root root 1367764 May 10 22:30 mpivars
    -rwxr-xr-x 1 root root    3430 May 10 22:30 parkill
    ```
include super-handy scripts like `mpif90` that set all of the include and library flags as part of running the appropriate compiler.
1. MPICH requires instructions on how to distribute work among the computing nodes.
This is most easily accomplished as a file known variously as a hostfile or machinefile or rankmap.
Mine is called `hostfile` and it looks like this:

    ```
    pi@hal:~ $ cat hostfile 
    192.168.2.11:4
    192.168.2.12:4
    192.168.2.13:4
    192.168.2.14:4
    192.168.2.1:2

    ```
I make available all four cores on each of my computing nodes for assignment.
I also offer two of the cores on the head node.
There are a variety of options for describing the cluster and distributing the load&mdash;consult the [MPICH](https://www.mpich.org/static/downloads/3.2.1/mpich-3.2.1-installguide.pdf) [documentation](https://wiki.mpich.org/mpich/index.php/Frequently_Asked_Questions).
We can test it using some example code that ships with MPICH.

    ```
    pi@hal:/mnt/nfs/shared/mpi $ mpif77 /usr/local/src/mpich-3.2.1/examples/f77/hellow.f 
    pi@hal:/mnt/nfs/shared/mpi $ mpiexec -n 16 -f ~/hostfile ./a.out 
     Process            0  of           16  is alive
     Process            1  of           16  is alive
     Process            2  of           16  is alive
     Process            4  of           16  is alive
     Process            3  of           16  is alive
     Process            5  of           16  is alive
     Process           12  of           16  is alive
     Process            6  of           16  is alive
     Process           13  of           16  is alive
     Process            7  of           16  is alive
     Process           14  of           16  is alive
     Process            8  of           16  is alive
     Process           15  of           16  is alive
     Process            9  of           16  is alive
     Process           10  of           16  is alive
     Process           11  of           16  is alive
    ```
**Success!**
At this point, ***you are done*** if all you want to do is run C, C++, and fortran programs.
1. **Note** that I build and run my programs on the nfs share so that all of the compute nodes have a copy of the executable at the same path as the head node.
If you don't have an nfs share, you'll need to copy the executable throughout the cluster.
The general merits of MPI and nfs (and of how much nfs) are discussed in [the FAQ of OpenMPI](https://www.open-mpi.org//faq/?category=building#where-to-install).
I decided to give every node their own copy of MPI, but I share a single `a.out` executable (and any associated input/output files) via nfs.
I also use the nfs share for .deb packages in order to avoid having every node download the same package during updates.
The `apt` sharing is accomplished by creating a directory on the shared partition of the head node

    ```
    pi@hal:~ $ sudo mkdir -p /mnt/nfs/shared/apt/archives
    ```
and then directing the head node and computing nodes to all look there for downloaded packages.
Create a file on *every* node in `/etc/apt/apt.conf.d/` named [`90nfs`](system/90nfs) that contains a single line

    ```
    pi@hal:~ $ cat /etc/apt/apt.conf.d/90nfs 
    Dir::Cache::Archives /mnt/nfs/shared/apt/archives;
    ```
If your [`/etc/exports`](system/exports) does not contain `no_root_squash`, this will not work.
1. If you'd like to also run python programs (like me), you'll need [`mpi4py`](http://mpi4py.scipy.org/docs/), as well.
It is version 3.0.0 at time of writing and the source tarball can be downloaded from [here](https://bitbucket.org/mpi4py/mpi4py/downloads/).
Unpack the tarball somewhere that you'd like it to live forever (I stupidly did it in Downloads) and build it.

    ```
    $ python3 setup.py build
    $ sudo python3 setup.py install
    ```
(There is also a test target which fails for me with a "Bus Error" after several seemingly successful tests.
I've not had any trouble, though.)
We can now test an example python program:

    ```
    pi@hal:/mnt/nfs/shared/mpi $ mpiexec -n 16 -f ~/hostfile python3 ~/Downloads/mpi4py-3.0.0/demo/helloworld.py 
    Hello, World! I am process 12 of 16 on hal04.
    Hello, World! I am process 13 of 16 on hal04.
    Hello, World! I am process 14 of 16 on hal04.
    Hello, World! I am process 15 of 16 on hal04.
    Hello, World! I am process 0 of 16 on hal01.
    Hello, World! I am process 1 of 16 on hal01.
    Hello, World! I am process 2 of 16 on hal01.
    Hello, World! I am process 3 of 16 on hal01.
    Hello, World! I am process 8 of 16 on hal03.
    Hello, World! I am process 9 of 16 on hal03.
    Hello, World! I am process 11 of 16 on hal03.
    Hello, World! I am process 10 of 16 on hal03.
    Hello, World! I am process 4 of 16 on hal02.
    Hello, World! I am process 5 of 16 on hal02.
    Hello, World! I am process 6 of 16 on hal02.
    Hello, World! I am process 7 of 16 on hal02.
    ```
**Success!**
1. I also wanted to try OpenMPI because I am not sure if MPICH will work with Java.
The version is 3.1.0 at time of writing.
I set up a similar PATH scheme as for MPICH.
Namely,

    ```
    pi@hal:~ $ sudo mkdir -p /home/openmpi/ompi-install
    pi@hal:~ $ cd /usr/local/src
    pi@hal:~ $ sudo tar -xjf openmpi-3.1.0.tar.bz2
    pi@hal:~ $ cd openmpi-3.1.0
    pi@hal:~ $ sudo ./configure --prefix=/home/openmpi/ompi-install --enable-mpi-fortran --enable-mpi-java --enable-mpi-cxx --with-jdk-bindir=/usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt/bin --with-jdk-headers=/usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt/include
    pi@hal:~ $ sudo make -j 2
    pi@hal:~ $ sudo make check
    pi@hal:~ $ sudo make install
    ```
where Java was not autodetected and so I had to pass it directly.
I ran the tests with the check target one or two times on each node and got varying results: from 3 FAILS to one perfect score&mdash;FYI.
**Do this same build and directory structure on the computing nodes.**
Once all of the computing nodes have finished the build and install, we can test it.

    ```
    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpicc ~/Downloads/mpi4py-3.0.0/demo/helloworld.c 
    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpiexec -machinefile nodefile -n 16 ./a.out 
    Hello, World! I am process 12 of 16 on hal04.
    Hello, World! I am process 0 of 16 on hal01.
    Hello, World! I am process 8 of 16 on hal03.
    Hello, World! I am process 13 of 16 on hal04.
    Hello, World! I am process 1 of 16 on hal01.
    Hello, World! I am process 9 of 16 on hal03.
    Hello, World! I am process 14 of 16 on hal04.
    Hello, World! I am process 2 of 16 on hal01.
    Hello, World! I am process 10 of 16 on hal03.
    Hello, World! I am process 5 of 16 on hal02.
    Hello, World! I am process 15 of 16 on hal04.
    Hello, World! I am process 3 of 16 on hal01.
    Hello, World! I am process 11 of 16 on hal03.
    Hello, World! I am process 6 of 16 on hal02.
    Hello, World! I am process 7 of 16 on hal02.
    Hello, World! I am process 4 of 16 on hal02.
    ```
**Success!**
So far, this works for fortran and C, but not for Java.

    ```
    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpif77 /usr/local/src/openmpi-3.1.0/examples/hello_mpifh.f 
    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpiexec -n 2 ./a.out 
    Hello, world, I am  0 of  2: Open MPI v3.1.0, package: Open MPI root@hal Distribution, ident: 3.1.0, repo rev: v3.1.0, May 07, 2018                                                                                                                                                         
    Hello, world, I am  1 of  2: Open MPI v3.1.0, package: Open MPI root@hal Distribution, ident: 3.1.0, repo rev: v3.1.0, May 07, 2018 
    
    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpicc /usr/local/src/openmpi-3.1.0/examples/hello_c.c 
    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpiexec -n 2 ./a.out 
    Hello, world, I am 0 of 2, (Open MPI v3.1.0, package: Open MPI root@hal Distribution, ident: 3.1.0, repo rev: v3.1.0, May 07, 2018, 103)
    Hello, world, I am 1 of 2, (Open MPI v3.1.0, package: Open MPI root@hal Distribution, ident: 3.1.0, repo rev: v3.1.0, May 07, 2018, 103)
    
    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpiexec -n 2 java Hello
    -------------------------------------------------------
    Primary job  terminated normally, but 1 process returned
    a non-zero exit code. Per user-direction, the job has been aborted.
    -------------------------------------------------------
    --------------------------------------------------------------------------
    mpiexec noticed that process rank 0 with PID 0 on node hal exited on signal 11 (Segmentation fault).
    --------------------------------------------------------------------------
    ```
Here is a more detailed failure of the same Java program run on the whole cluster.

    ```
    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpijavac -d . /usr/local/src/openmpi-3.1.0/examples/Hello.java 
    pi@hal:/mnt/nfs/shared/mpi $ /home/openmpi/ompi-install/bin/mpiexec -machinefile nodefile -n 16 java Hello
    #
    # A fatal error has been detected by the Java Runtime Environment:
    #
    # A fatal error has been detected by the Java Runtime Environment:
    #
    #  SIGSEGV (0xb) at pc=0x76fa4114, pid=31830, tid=1983300720
    #
    # JRE version: Java(TM) SE Runtime Environment (8.0_65-b17) (build 1.8.0_65-b17)
    # Java VM: Java HotSpot(TM) Client VM (25.65-b01 mixed mode linux-arm )
    # Problematic frame:
    # C  [libarmmem.so+0x4114]#
    #
    ```
Java otherwise works for compilation and execution on the system.
I'll write more when I know more.

# performance

![in action](images/IMG_0425_small.JPG) [![top](images/IMG_0426_small.JPG)](images/IMG_0426.JPG)

Above is a picture of the cluster working on a lengthier problem.
Plugged into the USB power hub is [an in-line voltage and current meter](https://www.amazon.com/Diymore-Charging-Detector-Voltmeter-Multimeter/dp/B01L6Y3IMK/) for the fourth computing node.
It reads 0.98 A and 5.24 V, so even when all four cores are crunching away, that Pi is only consuming 5 W.

Clicking on the image of the screen will bring up a full-resolution version in which you can read the `top` outputs from three of the four computing nodes.

So far, I have only run programs that shipped as examples with MPICH and mpi4py or that can be found at [this excellent site](http://people.sc.fsu.edu/~jburkardt/c_src/mpi/mpi.html) and also [this one](https://www.cs.swarthmore.edu/~newhall/cs87/s18/).

## Examples

Please see the code described [here](examples) for applications.
