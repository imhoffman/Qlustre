# Numerical Results and Example Code
Be sure to first read about the base system described on [the Qlustre page](https://gitlab.com/imhoffman/Qlustre) as well as its expansion described on [the upgrade page](../upgrade).

1) [numerical quadrature](quadrature)
1) [Mie scattering](Mie)
1) [Python-specific configurations](Python)

