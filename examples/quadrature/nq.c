#include<stdio.h>
#include<math.h>
#include<mpi.h>

// constants
long         N = 1048576L*256L;
double      pi = 3.141592653589793238462643383279502884;

//
// sub programs
//
void banner ( double A, double B, int Nproc ) {

 printf("\n");
 printf("  Numerically integrate f(x) = x^3 from A to B,\n");
 printf("\n");
 printf("  A        = %14.6g\n", A);
 printf("  B        = %14.6g\n", B);
 printf("  N        = %d\n", N);
 printf("\n");
 printf("  using MPI to divide the computation\n");
 printf("  among %d parallel processes.", Nproc-1);

 return;
}

// the integrand
double f ( double x ) {
 return x*x*x;
 // return 4.0*sqrt( 1.0 - x*x );
}


void boole ( double (*g)(double), long Npoints, double xmin, double xmax, double* area ) {
 double x, deltaX, h;
 long i;
 deltaX =(double) (xmax-xmin)/Npoints;
 h = deltaX/4.0;
 for ( i=1; i<=Npoints; i++ ) {
   x =(double) xmin + ( i-1 + 0.5 )*deltaX;
   *area = *area + h*2.0/45.0 * ( 
             7.0*g(x - h*2.0) + 32.0*g(x - h) + 12.0*g(x)
             + 32.0*g(x + h) +  7.0*g(x + h*2.0));
 }
 return;
}


void simpson ( double (*g)(double), long Npoints, double xmin, double xmax, double* area ) {
 double x, deltaX;
 long i;
 deltaX =(double) (xmax-xmin)/Npoints;
 for ( i=1; i<=Npoints; i++ ) {
   x =(double) xmin + ( i-1 + 0.5 )*deltaX;
   *area = *area + deltaX/8.0 * ( 
		    g( x - deltaX/2.0 )
              + 3.0*g( x - deltaX/6.0 )
              + 3.0*g( x + deltaX/6.0 )
		  + g( x + deltaX/2.0 ) );
 }
 return;
}

//
// main program
// 
int main(int argc, char *argv[]) {
  double x, A, B, pA, pB, subtotal=0.0, total=0.0, slice, wtime;
  long i, pN;
  int p, Nproc, mpiid, head, sendr, recvr, tag, err_flag=0;
  MPI_Status msgstatus;

  A =  -1.100;
  B =  +0.713;

  head = 0;

  err_flag += MPI_Init ( &argc, &argv );
  err_flag += MPI_Comm_size ( MPI_COMM_WORLD, &Nproc );
  err_flag += MPI_Comm_rank ( MPI_COMM_WORLD, &mpiid );
   
  if ( mpiid == head ) {
    wtime = MPI_Wtime();
    pN = N;
    banner ( A, B, Nproc );
  }

  // broadcast the number of points
  sendr = head;
  err_flag += MPI_Bcast ( &pN, 1, MPI_LONG, sendr, MPI_COMM_WORLD );

  if ( mpiid == head ) {

    for ( p=1; p<=Nproc-1; p++ ) {

      slice =(double) (B-A)/(Nproc-1);
      pA    =(double)  A  +  slice * (p-1);
      pB    =         pA  +  slice;

      // send out the assignments
      recvr = p;
      tag = 1;
      err_flag += MPI_Send ( &pA, 1, MPI_DOUBLE, recvr, tag, MPI_COMM_WORLD );
      tag = 2;
      err_flag += MPI_Send ( &pB, 1, MPI_DOUBLE, recvr, tag, MPI_COMM_WORLD );
    }
  } else {
    // receive the assignments
    sendr = head;
    tag = 1;
    err_flag += MPI_Recv ( &pA, 1, MPI_DOUBLE, sendr, tag, MPI_COMM_WORLD, &msgstatus );
    tag = 2;
    err_flag += MPI_Recv ( &pB, 1, MPI_DOUBLE, sendr, tag, MPI_COMM_WORLD, &msgstatus );

    // compute the area
    subtotal = 0.0;
    simpson ( &f, pN, pA, pB, &subtotal );
    // boole ( &f, pN, pA, pB, &subtotal );
  }

  // collect and sum the subtotals
  err_flag += MPI_Reduce ( &subtotal, &total, 1, MPI_DOUBLE, MPI_SUM, head, MPI_COMM_WORLD );

  if ( mpiid == head ) {
    wtime = MPI_Wtime() - wtime;
    printf("\n\n");
    printf(" Analytical = %32.24g\n", (B*B*B*B-A*A*A*A)/4.0 );
    printf("  Numerical = %32.24g\n", total );
    printf(" Total Time = %14.6g\n",  wtime );
  }

  err_flag += MPI_Finalize();
  if ( mpiid == head && err_flag == 0 ) {
    printf("\n  Normal end of execution.\n\n");
  }

  return 0;
}

