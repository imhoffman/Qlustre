## Quadrature

Quadrature is the classic embarassingly-parallel problem: the integral of a sum is the sum of the integrals.
[Here is fortran code](nq.f95) and [here is C code](nq.c) for computing the definite integral of x^3 using either Simpson's Rule or Boole's Rule.
Since both of those algorithms should yield exact results for polynomials of degree three or fewer, the cubic function is a valuable test case.

### fortran

One useful test for determining whether the quadrature algorithm has been encoded properly is to use zero and one as bounds: the area is a power of two and should be represented exactly for all practical floating-point precisions.

```
 
  Numerically integrate f(x) = x^3 from A to B,
 
  A        =   -1.00000    
  B        =    0.00000    
  N        =      1048576
 
  using MPI to divide the computation
  among 28 parallel processes.
 
 Analytical =  -0.250000000000000000000000    
  Numerical =  -0.249999999999999833466546    
       Time =   0.339344    
 
  Normal end of execution.
 
```

In our case, the numerical result differs in the 16th decimal place, as expected for quad precision (fortran `kind=real64`) on a 32-bit system.
It is important to note that even though [the ARM processors on the Raspberry Pi are 64-bit](https://en.wikipedia.org/wiki/ARM_Cortex-A53), the Raspbian operating system that we have chosen is only 32-bit.

I also happen to have an Odroid C2, which uses a comparable ARM Cortex-A53.
My C2 is running 64-bit Ubuntu 16.04, and so I compiled MPICH and ran this same code in parallel over three of the cores.
For the Odroid case, I altered the fortran code to use `kind=real128` (which is matched to the default `MPI_LONG_DOUBLE` for this platform&mdash;in general, you'd have to change that, too!).
The output has improved precision:

```
 
  Numerically integrate f(x) = x^3 from A to B,
 
  A        =   -1.00000    
  B        =    0.00000    
  N        =      1048576
 
  using MPI to divide the computation
  among  2 parallel processes.
 
 Analytical =  -0.250000000000000000000000000000000000    
  Numerical =  -0.250000000000000000000000000000000000    
       Time =    2.63733    
 
  Normal end of execution.
 
```

A more interesting test is to chose limits for which the result is not simply expressed in floating-point notation.
Here are the results from the Qlustre and Odroid, respectively:

The quad-precision result on the 32-bit Qlustre:

```
 
  Numerically integrate f(x) = x^3 from A to B,
 
  A        =   -1.10000    
  B        =   0.713000    
  N        =    268435456
 
  using MPI to divide the computation
  among 28 parallel processes.
 
 Analytical =  -0.301415239959750169695951    
  Numerical =  -0.301415239959750336229405    
 Total Time =    64.7576    
 
  Normal end of execution.
 
```

and on the 64-bit Odroid (albeit with the incorrect 32-bit literal...see below):

```
 
  Numerically integrate f(x) = x^3 from A to B,
 
  A        =   -1.10000    
  B        =   0.713000    
  N        =    268435456
 
  using MPI to divide the computation
  among  2 parallel processes.
 
 Analytical =  -0.301415271866101690428267844583750297    
  Numerical =  -0.301415271866101690428267844584048334    
       Time =    651.548    
 
  Normal end of execution.
 
```

### C

The C version of the same algorithm yields the following output on the cluster:

```

  Numerically integrate f(x) = x^3 from A to B,

  A        =           -1.1
  B        =          0.713
  N        = 268435456

  using MPI to divide the computation
  among 28 parallel processes.

 Analytical =      -0.301415239959750169695951
  Numerical =      -0.301415239959750336229405
 Total Time =        53.5814

  Normal end of execution.

```

~~which is a different answer than the fortran code.
I believe the C answer, so I need to track down why the fortran version is precise but not accurate.
The accuracy of the fortran code fails at around the eighth sig fig, so I suspect that there is a 32-bit float compromising the result somewhere.~~
(Indeed, I had entered the integration limits in the fortran code as `E` rather than `D` or `Q`, as is needed.
With proper literals, the fortran and C results agree.
Be sure to edit the code in the repo to suit your system.)

After switching the floats from `double` to `long double` and entering the literals with an `L` suffix and changing the message pass to `MPI_LONG_DOUBLE`, here is the output on the Odroid:

```

  Numerically integrate f(x) = x^3 from A to B,

  A        =           -1.1
  B        =          0.713
  N        = 268435456

  using MPI to divide the computation
  among 2 parallel processes.

 Analytical =      -0.301415239959750000000000000000000044
  Numerical =      -0.301415239959750000000000000000029318
 Total Time =        678.617

  Normal end of execution.

```

