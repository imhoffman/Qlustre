! modified from http://people.sc.fsu.edu/~jburkardt/f_src/quad_mpi/quad_mpi.html
 module types
   use iso_fortran_env
   implicit none
   ! bit widths; changing these requires changing MPI_LONG_DOUBLE, MPI_INT, etc.
   ! or else use PROBE and COUNT, etc.
   !  eg http://www.mathcs.emory.edu/~cheung/Courses/355/Syllabus/92-MPI/send+recv-adv.html
   integer, parameter :: rw = real64
   integer, parameter :: iw = int32
   real ( kind = rw ), parameter :: pi = 3.141592653589793238462643383279502884D0
   !real ( kind = rw ), parameter :: pi = 3.141592653589793238462643383279502884Q0
   integer ( kind = iw ), parameter :: N = 1048576*256
 end module types

 ! subprograms
 module subs
   use types
   implicit none
   contains
   subroutine banner ( N, A, B, Nproc )
     implicit none
     integer (kind=iw), intent(in) :: N
     integer, intent(in)           :: Nproc
     real (kind=rw), intent(in)    :: A, B
     write ( *, '(a)' ) ' '
     write ( *, '(a)' ) '  Numerically integrate f(x) = x^3 from A to B,'
     write ( *, '(a)' ) ' '
     write ( *, '(a,g14.6)' ) '  A        = ', A
     write ( *, '(a,g14.6)' ) '  B        = ', B
     write ( *, '(a,i12)' )   '  N        = ', N
     write ( *, '(a)' )       ' '
     write ( *, '(a)' )       '  using MPI to divide the computation'
     write ( *, '(a,i2,a)' )  '  among ',Nproc-1,' parallel processes.'
    return
   end subroutine banner

   function f ( x )
    implicit none
    real ( kind = rw ) :: f
    real ( kind = rw ), intent(in) :: x
    f = x*x*x
    ! f = real(4.0E0,kind=rw) * sqrt( real(1.0E0,kind=rw) - x*x )
    return
   end function f

   ! Boole's Rule
   subroutine boole ( g, Npoints, xmin, xmax, area )
     implicit none
     integer ( kind = iw ), intent(in) :: Npoints
     real ( kind = rw ), intent(in)    :: xmin, xmax
     procedure ( f ), pointer          :: g
     real ( kind = rw )                :: x, deltaX, h, area
     integer ( kind = iw )             :: i

     deltaX = (xmax-xmin)/real(Npoints,kind=rw)
     h = deltaX/real(4,kind=rw)
     do i = 1, Npoints
       x = xmin + ( real(i-1,kind=rw) + real( 0.5D0 ,kind=rw) )*deltaX
       area = area + h*real(2,kind=rw)/real(45,kind=rw) * ( &
            &      real(7,kind=rw)*g(x - h*real(2,kind=rw)) &
            &   + real(32,kind=rw)*g(x - h)                 &
            &   + real(12,kind=rw)*g(x)                     &
            &   + real(32,kind=rw)*g(x + h)                 &
            &   +  real(7,kind=rw)*g(x + h*real(2,kind=rw)) &
            &       )
     end do
     return
   end subroutine boole

   ! Simpson's 3/8 Rule
   subroutine simpson ( g, Npoints, xmin, xmax, area )
     implicit none
     integer ( kind = iw ), intent(in) :: Npoints
     real ( kind = rw ), intent(in)    :: xmin, xmax
     procedure ( f ), pointer          :: g
     real ( kind = rw )                :: x, deltaX, area
     integer ( kind = iw )             :: i

     deltaX = (xmax-xmin)/real(Npoints,kind=rw)
     do i = 1, Npoints
       x = xmin + ( real(i-1,kind=rw) + real( 0.5D0 ,kind=rw) )*deltaX
       area = area + deltaX/real(8,kind=rw) * ( &
            &                    g( x - deltaX/real(2,kind=rw) ) &
            &  + real(3,kind=rw)*g( x - deltaX/real(6,kind=rw) ) &
            &  + real(3,kind=rw)*g( x + deltaX/real(6,kind=rw) ) &
            &                  + g( x + deltaX/real(2,kind=rw) ) &
            &       )
     end do
     return
   end subroutine simpson

   ! the routine that came with the Burkardt code
   subroutine rect ( g, my_N, my_a, my_b, area )
     implicit none
     integer ( kind = iw )          :: my_N
     real ( kind = rw ), intent(in) :: my_a, my_b
     procedure ( f ), pointer       :: g
     real ( kind = rw )             :: x, area
     integer ( kind = iw )          :: i

     do i = 1, my_N
       x = ( real ( my_N - i,     kind = rw ) * my_a   &
           + real (        i - 1, kind = rw ) * my_b ) &
           / real ( my_N     - 1, kind = rw )
       area = area + g ( x )
     end do

     area = ( my_b - my_a ) * area / real ( my_N, kind = rw )
     return
   end subroutine rect
 end module subs

 ! main
 program main
   use types
   use subs
   use mpi
   implicit none

   real ( kind = rw )       :: x, A, B, pA, pB
   real ( kind = rw )       :: subtotal=0.0, total=0.0, slice
   integer ( kind = iw )    :: i, pN
   integer ( kind = 4 )     :: p, Nproc, mpiid
   integer ( kind = 4 )     :: head, sendr, recvr, tag, error_flag
   integer ( kind = 4 )     :: msgstatus(MPI_STATUS_SIZE)
   real ( kind = 8 )        :: wtime
   procedure ( f ), pointer :: fp => f

   A =  real( -1.100D+00 ,kind=rw)
   B =  real( +0.713D+00 ,kind=rw)

   ! hostfile has one proc on hal listed first
   head = 0

   call MPI_Init ( error_flag )
   call MPI_Comm_size ( MPI_COMM_WORLD, Nproc, error_flag )
   call MPI_Comm_rank ( MPI_COMM_WORLD, mpiid, error_flag )
   
   if ( mpiid == 0 ) then
     wtime = MPI_Wtime ( )

     ! pN = N / ( Nproc - 1 )
     ! N = ( Nproc - 1 ) * pN
     pN = N

     call banner ( N, A, B, Nproc )
  end if

  ! broadcast the number of points
  sendr = head
  call MPI_Bcast ( pN, 1, MPI_LONG, sendr, MPI_COMM_WORLD, error_flag )
!  call MPI_Bcast ( pN, 1, MPI_INT, sendr, MPI_COMM_WORLD, error_flag )

  if ( mpiid == 0 ) then

    ! break up the domain for the different processes
    do p = 1, Nproc-1

      ! each parallel proc is responsible for one slice of the domain
      slice = (B-A)/real(Nproc-1,kind=rw)
      pA =  A  +  slice * real(p-1,kind=rw)
      pB = pA  +  slice

      ! send out the assignments
      recvr = p
      tag = 1
      call MPI_Send ( pA, 1, MPI_LONG_DOUBLE, &
                    & recvr, tag, MPI_COMM_WORLD, error_flag )
      tag = 2
      call MPI_Send ( pB, 1, MPI_LONG_DOUBLE, & 
                    & recvr, tag, MPI_COMM_WORLD, error_flag )
    end do
  else
    ! receive the assignments
    sendr = head
    tag = 1
    call MPI_Recv ( pA, 1, MPI_LONG_DOUBLE, sendr, tag, &
      MPI_COMM_WORLD, msgstatus, error_flag )
    tag = 2
    call MPI_Recv ( pB, 1, MPI_LONG_DOUBLE, sendr, tag, &
      MPI_COMM_WORLD, msgstatus, error_flag )
    ! write(6,'(A,I3,A,G10.3,A,G10.3)') ' proc',mpiid,' has limits ',pA,' and ',pB

    ! compute the area
    subtotal = real( 0.0D+00, kind = rw )
    call simpson ( fp, pN, pA, pB, subtotal )
!    call boole ( fp, pN, pA, pB, subtotal )
!    call rect ( fp, pN, pA, pB, subtotal )
  end if

  ! collect and sum the subtotals
  call MPI_Reduce ( subtotal, total, 1, MPI_LONG_DOUBLE, &
    MPI_SUM, head, MPI_COMM_WORLD, error_flag )

  if ( mpiid == head ) then
    wtime = MPI_Wtime ( ) - wtime
    write(6,'(a)') ' '
    write(6,'(a,g32.24)') ' Analytical = ', (B*B*B*B-A*A*A*A)/real(4,kind=rw)
    write(6,'(a,g32.24)') '  Numerical = ', total
    write(6,'(a,g14.6)')  ' Total Time = ', wtime
  end if

  call MPI_Finalize ( error_flag )
  if ( mpiid == head .and. error_flag == 0 ) then
    write(6,'(a)') ' '
    write(6,'(a)') '  Normal end of execution.'
    write(6,'(a)') ' '
  end if

  stop
 end program main
