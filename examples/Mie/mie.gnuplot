# http://ayapin-film.sakura.ne.jp/Gnuplot/Docs/ps_guide.pdf
##
# for use with mie.f90 qlustre code
filename = "mie.dat"
## parse header
r = system("head -1 " . filename . " | awk '{printf \"%.8f\", $1}'")
r = r + 0.0
nm = system("head -1 " . filename . " | awk '{printf \"%.8f\", $2}'")
nm = nm + 0.0
nsr = system("head -1 " . filename . " | awk '{printf \"%.8f\", $3}'")
nsr = nsr + 0.0
nsi = system("head -1 " . filename . " | awk '{printf \"%.8f\", $4}'")
nsi = nsi + 0.0
##
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 24" linewidth 2
set xtics font ",18"
set ytics font ",18"
set key inside top left
set output 'gnuplot.png'
set xzeroaxis
set yzeroaxis
set label sprintf("{/:Italic r}_{sphere} = %#.3g {/Symbol m}m", r) at graph 0.55,0.90
set label sprintf("{/:Italic n}_{sphere} = %#.3g + {/:Italic i}%#.3g", nsr, nsi) at graph 0.55,0.82
set label sprintf("{/:Italic n}_{medium} = %#.3g", nm) at graph 0.55,0.74
set xlabel 'wavelength, {/Symbol l} (nm)'
set ylabel '{/:Italic Qback} (arb)'
plot 'mie.dat' every ::1 using 1:2 with lines title ''
