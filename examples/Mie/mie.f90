!
!  Mie scattering using BHMIE --- http://scatterlib.wikidot.com/mie
!  for use on the Qlustre --- https://gitlab.com/imhoffman/Qlustre
!
 module types
   use iso_fortran_env
   implicit none
   ! bit widths; changing these requires changing MPI_LONG_DOUBLE, MPI_INT, etc.
   ! or else use PROBE and COUNT, etc.
   !  eg http://www.mathcs.emory.edu/~cheung/Courses/355/Syllabus/92-MPI/send+recv-adv.html
   integer, parameter :: rw = real32     ! limited by F77 BHMIE code
   integer, parameter :: iw = int32
   real ( kind = rw ), parameter :: pi = 3.141592653589793238462643383279502884D0
   integer ( kind = iw ), parameter :: Ncores = 28     ! computing cores w/o the head
   integer ( kind = iw ), parameter :: N = 2048*Ncores
 end module types

 ! subprograms
 module subs
   use types
   implicit none
   contains
   subroutine banner ( N, Nproc, radius, Lmin, Lmax )
     implicit none
     integer (kind=iw), intent(in) :: N
     integer, intent(in)           :: Nproc
     real (kind=rw), intent(in)    :: radius, Lmin, Lmax
     write ( *, '(/,a)' )              '  Mie scattering of simple spheres.'
     write ( *, '(a,F0.4)' )         '  sphere radius (microns): ', radius/1.E-06
     write ( *, '(a,F0.4,2X,F0.4)' ) '  wavelength range (nm): ', Lmin/1.E-09, Lmax/1.E-09
     write ( *, '(a)' )              '  Outputs to mie.dat for plotting.'
     write ( *, '(/,2X,I0,a,I2,a,/)' )   N, ' array values to be split ', Nproc-1, ' ways.' 
    return
   end subroutine banner
 end module subs

 !
 !  main program
 !
 program main
   use types
   use subs
   use mpi
   implicit none
   external bhmie

   real ( kind = rw )     :: Lmin, Lmax, DeltaL
   real ( kind = rw )     :: x, radius, n_medium, Qabs, Qback, Qext, Qsca, Gsca
   complex ( kind = rw )  :: n_sphere, n_ratio, s1(32), s2(32)
   integer ( kind = iw )  :: i, pN=N/Ncores
   integer                :: p, Nproc, mpiid
   integer                :: head=0, sendr, recvr, error_flag
   integer                :: msgstatus(MPI_STATUS_SIZE)
   real ( kind = 8 )      :: wtime
   real(kind=rw), dimension(:,:), allocatable :: master_2D_array
   real(kind=rw), dimension(:), allocatable   :: lambda_1D_array
   real(kind=rw), dimension(:), allocatable   :: xs_1D_array
   real(kind=rw), dimension(:), allocatable   :: p_lambda_1D_array
   real(kind=rw), dimension(:), allocatable   :: p_xs_1D_array
   integer,       dimension(:), allocatable   :: displs
   integer,       dimension(:), allocatable   :: sendcounts

   allocate( master_2D_array( 2, N ) )
   allocate( lambda_1D_array( N ) )
   allocate( xs_1D_array( N ) )
   allocate( p_lambda_1D_array( pN ) )
   allocate( p_xs_1D_array( pN ) )

   Lmin     = real(  +350.0D-09 ,kind=rw)    ! lower wavelength limit
   Lmax     = real(  +1125.D-09 ,kind=rw)    ! upper wavelength limit
   radius   = real(  +0.470E-06 ,kind=rw)    ! radius of scattering spheres 
   n_medium = real(  +1.33D0    ,kind=rw)    ! index of refraction of medium
   n_sphere = cmplx( 1.47D0, 0.0D0 ,kind=rw) ! index of refraction of spheres
   n_ratio = n_sphere/n_medium
   DeltaL = ( Lmax - Lmin )/real(N,kind=rw)

   do i = 1, N
    master_2D_array(1,i) = Lmin + real(i-1,kind=rw)*DeltaL
    lambda_1D_array(i) = master_2D_array(1,i)     ! must flatten arrays for scattering
    xs_1D_array(i) = master_2D_array(2,i)         ! must flatten arrays for scattering
   enddo

   ! hostfile has one proc on hal listed first, so mpiid 0 should be one core on the head
   call MPI_Init ( error_flag )
   call MPI_Comm_size ( MPI_COMM_WORLD, Nproc, error_flag )
   call MPI_Comm_rank ( MPI_COMM_WORLD, mpiid, error_flag )

   ! assemble parameter arrays for Scatterv and Gatherv
   allocate( displs( Nproc ) )
   allocate( sendcounts( Nproc ) )
   displs(1) = 0       ! the head node gets none of the work array
   displs(2) = 0       ! the head node gets none of the work array
   sendcounts(1) = 0   ! the head node gets none of the work array
   sendcounts(2) = pN
   do i = 3, Nproc
     displs(i) = pN*(i-2)
     sendcounts(i) = pN
   enddo
  
   ! start the clock on the job 
   if ( mpiid .eq. head ) then
     wtime = MPI_Wtime ( )
     call banner ( N, Nproc, radius, Lmin, Lmax )
   end if

   ! scatter the flattened pieces of the master array
   !  MPI_REAL is needed since BHMIE uses F77 32-bit REAL
   sendr = head
   call MPI_Scatterv( lambda_1D_array, sendcounts, displs, MPI_REAL, &
            p_lambda_1D_array, pN, MPI_REAL, sendr, MPI_COMM_WORLD, error_flag )
   call MPI_Scatterv( xs_1D_array, sendcounts, displs, MPI_REAL, &
            p_xs_1D_array, pN, MPI_REAL, sendr, MPI_COMM_WORLD, error_flag )
   !write(6,*) ' error flag:', error_flag

   !  run bhmie with the array values on the computing cores
   if ( mpiid .ne. head ) then
     do i = 1, pN
       x = 2.0D0 * pi * radius * n_medium / p_lambda_1D_array(i)
       call bhmie( x, n_ratio, 2, s1, s2, Qext, Qsca, Qback, Gsca )
       p_xs_1D_array(i) = Qback
     enddo
   end if

   ! gather up the arrays
   recvr = head
   call MPI_Gatherv( p_lambda_1D_array, pN, MPI_REAL, lambda_1D_array, &
           sendcounts, displs, MPI_REAL, recvr, MPI_COMM_WORLD, error_flag )
   call MPI_Gatherv( p_xs_1D_array, pN, MPI_REAL, xs_1D_array, &
           sendcounts, displs, MPI_REAL, recvr, MPI_COMM_WORLD, error_flag )

   ! load the flattened arrays back into the master array
   ! write data to file for plotting
   if ( mpiid .eq. head ) then
     open( unit=11, file="mie.dat", status="unknown" )
     ! write header line for gnuplot
     write(11,*) radius/1.E-06, n_medium, real(n_sphere), aimag(n_sphere)
     do i = 1, N
       master_2D_array(1,i) = lambda_1D_array(i)
       master_2D_array(2,i) = xs_1D_array(i)
       write(11,*) lambda_1D_array(i)/1.0E-09, xs_1D_array(i)
     enddo
     close(11)
   endif


   !
   ! close down
   !
   if ( mpiid .eq. head ) then
     wtime = MPI_Wtime ( ) - wtime
     write(6,'(a,g14.6)')  ' Total time (sec): ', wtime
   end if

   call MPI_Finalize ( error_flag )
   if ( mpiid .eq. head .and. error_flag .eq. 0 ) then
     write(6,'(/,a,/)') '  Normal end of execution.'
   end if

   deallocate( master_2D_array )
   deallocate( lambda_1D_array )
   deallocate( xs_1D_array )
   deallocate( p_lambda_1D_array )
   deallocate( p_xs_1D_array )
   deallocate( displs )
   deallocate( sendcounts )

   stop
 end program main
