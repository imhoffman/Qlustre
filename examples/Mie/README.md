# Mie Scattering

## Simple spheres

Based on [the well-known Fortran BHMIE code](http://scatterlib.wikidot.com/mie), the following plot is obtained using [`mie.f90`](mie.f90).

```
$ make
$ mpiexec -n 29 -f ../../../hostfile.29 ./a.out
$ gnuplot mie.gnuplot
```

![gnuplot.png](gnuplot.png)

## Clusters of spheres

The simple spheres doesn't really require cluster computing.
It was all practice for the more complicated problem that is needed for a research project.
~~The plan is to use [GMM from here](http://scatterlib.wikidot.com/multispheres).~~
Actually, [MSTM](http://eng.auburn.edu/users/dmckwski/scatcodes/) is more flexible than I originally thought.
Plus, MSTM is already ported to MPICH! ... although it hangs when I run the packaged examples ...

