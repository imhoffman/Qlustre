# MPI in Python
Python is not a high-performance language, so one should not expect it to perform.
Indeed, much of Python's success can be attributed to the fact that abundant computing resources have been cheap and fast for the last 20 years.
It follows, then, that throwing cluster resources at slow Python processes is appropriate, as long as we don't compare the results to what we could have gotten out of Fortran or C.

## Software configuration
As of writing (July 2021), I'm still using the stock `stretch` Raspbian OS from my initial install.
I update it faithfully, which means that I have GCC 6.3.0 and Python 3.5.3.
I have not upgraded my MPICH from the initial 3.2.1.

What I want to do is to install an additional, newer version of Python on my `nfs` share so that users can run MPI jobs from their environment on the head node.
The notes here are for my install of Python 3.7.11 on top of Raspbian's 3.5.3.
Then, I had to build `NumPy` locally since there was [a `libc` mismatch](https://stackoverflow.com/questions/56629793/importerror-lib-arm-linux-gnueabihf-libc-so-6-version-glibc-2-28-not-found) between the Python that I had just built from source and that of the `NumPy` wheel that `pip` was downloading.

The wheel build of `NumPy` is more than simply character-building; I want to know what I'll need to do in order to deploy an [Intel-compiled source build of Python 3.9](https://software.intel.com/content/www/us/en/develop/articles/build-numpy-with-mkl-and-icc.html) on a different HPC system.

## Source builds

### System binaries
After fetching and unpacking [the source tarball for 3.7.11](https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz), I configured and built as the primary `pi` user as follows:

```
$ ./configure --with-assertions --with-lto --enable-optimizations --prefix=/mnt/nfs/shared/local
$ make -j4 all && make -j4 test
$ make -j4 altinstall
```

Thus, the system install is on the nfs share, so all of the worker nodes can see the binaries.
I added the nfs directory to the system path:

```
$ cat /etc/profile.d/nfs_local.sh 
export PATH=$PATH:/mnt/nfs/shared/local/bin
```

Also as the `pi` user, I upgraded `pip`, `setuptools`, and `wheel` in the system install:

```
$ python3.7 -m pip install --upgrade pip setuptools wheel
```

Since a `NumPy` build will need `Cython`, I also wanted to install `Cython` and for the install to use my system libraries, so I built it locally and installed the resulting wheel:

```
$ python3.7 -m pip wheel --no-binary Cython Cython
$ python3.7 -m pip install Cython-0.29.23-cp37-cp37m-linux_armv7l.whl
```

### User environment
As an individual user, I installed the modules for my environment.
In order for the [user installs](https://pip.pypa.io/en/latest/user_guide/#user-installs) to be visible by the worker nodes, I had to install the user binaries on the nfs share.
I created a directory in my personal space and set the appropriate [environment variable](https://docs.python.org/3/using/cmdline.html#environment-variables):

```
$ export PYTHONUSERBASE=/mnt/nfs/shared/mpi/imh/.local
```

With this as the destination, I built `NumPy` form source and installed the resulting wheel:

```
$ python3.7 -m pip wheel --no-binary numpy numpy
$ python3.7 -m pip install --user numpy-1.21.0-cp37-cp37m-linux_armv7l.whl
```

For `scipy` and `pandas`, the wheels available from `pip` are suitable, so

```
$ python3.7 -m pip install --user scipy pandas
```

As one final local build, I got the most recent release of `mpi4py`.
Since [`mpicc` is already in my path](https://mpi4py.readthedocs.io/en/stable/install.html#using-pip-or-easy-install), this was simply:

```
$ python3.7 -m pip wheel --no-binary mpi4py mpi4py
$ python3.7 -m pip install --user mpi4py-3.0.3-cp37-cp37m-linux_armv7l.whl
```

## Testing
Since the workers do not execute a login shell when tasked, the environment variables described above must be passed explicitly.

For example, to ensure that the head node is properly serving the binaries to the workers, a good test is to start Python via `ssh`:

```
$ ssh imh@hal06 "PYTHONUSERBASE=/mnt/nfs/shared/mpi/imh/.local PATH=$PATH:/mnt/nfs/shared/local/bin python3.7 -c 'import mpi4py; print(mpi4py.__version__)'"
3.0.3

$ ssh imh@hal02 "PYTHONUSERBASE=/mnt/nfs/shared/mpi/imh/.local PATH=$PATH:/mnt/nfs/shared/local/bin python3.7 -c 'import numpy; print(numpy.__version__)'"
1.21.0
```

For MPI tests, [the module provides a few](https://mpi4py.readthedocs.io/en/stable/install.html#testing) is good:

```
$ mpiexec -n 30 -f /mnt/nfs/shared/mpi/hostfile -env PYTHONUSERBASE /mnt/nfs/shared/mpi/imh/.local -env PATH $PATH:/mnt/nfs/shared/local/bin python3.7 -m mpi4py.bench helloworld
Hello, World! I am process  0 of 30 on hal01.
Hello, World! I am process  1 of 30 on hal01.
...
Hello, World! I am process 11 of 30 on hal03.
Hello, World! I am process 12 of 30 on hal04.
...
```

```
$ mpiexec -n 30 -f /mnt/nfs/shared/mpi/hostfile -env PYTHONUSERBASE /mnt/nfs/shared/mpi/imh/.local -env PATH $PATH:/mnt/nfs/shared/local/bin python3.7 -m mpi4py.bench ringtest -l 128 -n 1024
time for 128 loops = 0.282203 seconds (30 processes, 1024 bytes)
```

NB: If you get an error from `launch_procs` that it is unable to change `wdir`, it is because you are launching from a directory that only exists on the head node.
Be sure to start from the nfs share that is visible to all of the workers.

