from ff_module import C_quad
from timeit import default_timer as stamp
from math import cos
from numpy import arange
from mpi4py.futures import MPIPoolExecutor
from mpi4py import MPI
from functools import partial

##
##  the domain of the integral and
##   the number of points in each numerical sum
##
xmin =   -0.1
xmax =   +0.9
Npoints = 1024


##
##  it is important to use a tuple for the parameters here
##   rather than a list because it will be the key in the
##   dictionary of returned values; lists can't be keys
##
coeffs_search_list = [ ( A, B, C )
                       for A in arange( 0.1, 10.0, 0.1 )
                       for B in arange( 0.1, 10.0, 0.1 )
                       for C in arange( 0.1, 10.0, 0.1 ) ]

f = partial( C_quad, xmin, xmax, Npoints )


##
##  pool search through parameter space
##   `max_workers` seems to be the number of MPI jobs per node
##
if __name__ == '__main__':
    start_time = stamp()
    results_dict = {}
    with MPIPoolExecutor(max_workers=2) as ex:
        for result, params in ex.map( f, coeffs_search_list, chunksize=7, unordered=True ):
            results_dict[params] = result

    ##
    ##  look for, say, the closest to zero
    ##
    if MPI.COMM_WORLD.Get_rank() == 0:
        least_negative_result = -1E8
        least_positive_result = +1E8
        best_key_negative = None
        best_key_positive = None
        for k in coeffs_search_list:
            if results_dict[k] >= 0.0 and results_dict[k] < least_positive_result:
                least_positive_result = results_dict[k]
                best_key_positive = k
            if results_dict[k] <= 0.0 and results_dict[k] > least_negative_result:
                least_negative_result = results_dict[k]
                best_key_negative = k
        if best_key_negative is None:
            best_key = best_key_positive
        elif best_key_positive is None:
            best_key = best_key_negative
        elif abs( results_dict[best_key_negative] ) < results_dict[best_key_positive]:
            best_key = best_key_negative
        else:
            best_key = best_key_positive

        print( f'\n best params in search (A,B,C): {best_key}\n' )

        print( f' numerical quadrature of {results_dict[best_key]:g} found in {(stamp()-start_time)/60.0:.1f} minutes\n' )

        coeffs = best_key
        manual = -1.0/coeffs[0]*cos( coeffs[0]*xmax - coeffs[2] ) \
                + coeffs[1]/2.0*xmax*xmax \
                + 1.0/coeffs[0]*cos( coeffs[0]*xmin - coeffs[2] ) \
                - coeffs[1]/2.0*xmin*xmin

        print( f' analytical expectation: {manual:g} for given params\n' )


