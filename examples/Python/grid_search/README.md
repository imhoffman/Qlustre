# MPI/OMP example
This is an example that uses a `ctypes` OpenMP foreign-function that is distributed via an MPI pool.
As a toy example, a grid search is performed.
The jobs in the grid are embarrassingly parallel, so no distributed-memory messages are passed.


## the foreign-function library

### the C code
The function `sin(A*x-C)+B*x` was chosen arbitrarily.
The file [`ff_code.c`](ff_code.c) was compiled into a library as follows:

```
$ gcc -c -o fflib.o -O3 -fopenmp ff_code.c -lm && gcc -fopenmp -shared -o fflib.so fflib.o
```

### the interface module
The wrapper function returns not only the `double` that is returned from C, but also the params that were passed out, as explained below.


## the grid-search script
For a grid of values for A, B, and C, the function is integrated and the area reported.
The set of parameters that provides the area closest to zero is printed at the conclusion of the search.

For this script, an asynchronous pool of jobs is used.
The strategy is based on [this discussion in the mpi4py forum](https://groups.google.com/g/mpi4py/c/k7Hc6raaWgY).
As mentioned in [the docs](https://mpi4py.readthedocs.io/en/stable/mpi4py.futures.html#mpipoolexecutor), I set the `chunksize` to a favorable value.

Since I am using `unsorted=True`, I am not sure how to know which item in the iterable corresponds to which return.
So, I return the input params as part of the output so that I have both in hand at once.

Owing to my choice for `OMP_NUM_THREADS`, I set `max_workers=2` for my cluster topology, as described below.
Here is the complete launch command (see the [configuration page](../README.md) for details on the environment):

```
$ mpiexec -n 7 -f ../hostfile-omp -env PYTHONUSERBASE /mnt/nfs/shared/mpi/imh/.local -env PATH $PATH:/mnt/nfs/shared/local/bin -env OMP_NUM_THREADS 2 python3.7 search.py 

 best params in search (A,B,C): (6.0, 0.1, 9.700000000000001)

 numerical quadrature of -4.54092e-06 found in 21.7 minutes

 analytical expectation: -4.54092e-06 for given params

```

In this case, each job was given two OMP threads.
Since each node has four cores, the number of workers set in the executor pool is 2, so that each node gets two two-core jobs.
For example, here is a shot of one of the worker nodes during the computation:

![top_grid_search.png](top_grid_search.png)


