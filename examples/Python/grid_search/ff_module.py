import ctypes
import os
import numpy as np

_fflib = ctypes.CDLL( os.path.join( os.path.dirname(__file__), 'fflib.so' ) )


##
##  sets the C types of the arguments of the function in the library
##   these must be declared as part of the interface
##   these must be changed here if they are changed in ff_code.c
##   there is no need to communicate C `const` or C `restrict` to Python
##
_fflib.ffnq.argtypes = ( ctypes.c_double,
                          ctypes.c_double,
                          ctypes.c_long,
                          ctypes.POINTER( ctypes.c_double ) )

_fflib.ffnq.restype  = ctypes.c_double


##
##  the Python routine that makes the call out to the C library
##
def C_quad ( xmin, xmax, Npoints, coeffs ):

    ##
    ##  ensure memory layout and obtain double* address for the list
    ##
    coeffs_addr = np.ascontiguousarray( coeffs, dtype=ctypes.c_double ).ctypes.data_as( ctypes.POINTER( ctypes.c_double ) )

    ##
    ##  pass inputs to C function
    ##
    result = _fflib.ffnq(
                        ctypes.c_double( xmin ),
                        ctypes.c_double( xmax ),
                        ctypes.c_long( Npoints ),
                        coeffs_addr
                        )

    ##
    ##  when using an MPI executor pool, it is not clear to me how to
    ##   determine the pool job that resulted in a given return, so
    ##   I am returning the parameters along with the result here and
    ##   then catching them upon return of the pool job so that I can
    ##   associate them
    ##
    return result, coeffs

