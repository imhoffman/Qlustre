#include<stdio.h>
#include<math.h>
#include<omp.h>

//
//  $ gcc -c -o fflib.o -O3 -fopenmp ff_code.c -lm
//  $ gcc -fopenmp -shared -o fflib.so fflib.o
//

//
//  I have GCC 6.3 on my cluster, which automatically shares
//   `const` args into the omp pragma; for GCC 9+, the
//   standard compliant behavior is used, so explicitly
//   share them
//

//
//  sub programs
//
//   the integrand
double
f ( const double x,
    const double *const coeffs )
{
 return sin( coeffs[0]*x - coeffs[2] ) + coeffs[1]*x;
}


//  Boole's Rule summation
void
boole ( double (*g)(const double, const double *const),
	const double *const C,
	const long Npoints, 
	const double xmin,
	const double xmax, 
	double* area )
{
 const double deltaX =(double) (xmax-xmin)/Npoints;
 const double h = deltaX/4.0;
 double temp;
 #pragma omp parallel for \
             shared  ( g ) \
             default ( none ) \
             schedule( static ) \
             reduction( +:temp )
 for ( long i=1L; i<=Npoints; i++ ) {
   const double x =(double) xmin + ( i-1L + 0.5 )*deltaX;
   temp += h*2.0/45.0
	   * (   7.0*g(x - h*2.0,C) + 32.0*g(x - h,C)    + 12.0*g(x,C)
              + 32.0*g(x + h,C)     +  7.0*g(x + h*2.0,C));
 }
 *area = temp;
 return;
}


//  Simpson's Rule summation
void
simpson ( double (*g)(const double, const double *const),
	  const double *const C,
	  const long Npoints, 
	  const double xmin, 
	  const double xmax,
	  double* area )
{
 const double deltaX =(double) (xmax-xmin)/Npoints;
 double temp;
 #pragma omp parallel for \
             shared  ( g ) \
             default ( none ) \
             schedule( static ) \
             reduction( +:temp )
 for ( long i=1L; i<=Npoints; i++ ) {
   const double x =(double) xmin + ( i-1L + 0.5 )*deltaX;
   temp +=  deltaX/8.0 * ( 
		    g( x - deltaX/2.0, C )
              + 3.0*g( x - deltaX/6.0, C )
              + 3.0*g( x + deltaX/6.0, C )
		  + g( x + deltaX/2.0, C ) );
 }
 *area = temp;
 return;
}


//
//  the library function that is called from Python
// 
__attribute__((visibility("default")))
double
ffnq ( const double xmin,
       const double xmax,
       const long   Npoints,
       const double *const coeffs )
{
  //fprintf( stdout, " ffnq called with A,B,C=%.2f,%.2f,%.2f\n", coeffs[0], coeffs[1], coeffs[2] );

  double total;

  total = 0.0;
  simpson ( &f, coeffs, Npoints, xmin, xmax, &total );
  //boole ( &f, coeffs, Npoints, xmin, xmax, &total );

  return total;
}

