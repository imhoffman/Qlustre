from ff_module import C_quad
from timeit import default_timer as stamp
from math import cos

xmin =   0.2
xmax =   5.3
Npoints = 2048
coeffs = [ 9.6, 2.3, 1.5 ]

start_time = stamp()

answer,_ = C_quad( xmin, xmax, Npoints, coeffs )

print( f'\n numerical quadrature of {answer:g} obtained in {stamp() - start_time:.4f} seconds\n' )


manual = -1.0/coeffs[0]*cos( coeffs[0]*xmax - coeffs[2] ) \
        + coeffs[1]/2.0*xmax*xmax \
        + 1.0/coeffs[0]*cos( coeffs[0]*xmin - coeffs[2] ) \
        - coeffs[1]/2.0*xmin*xmin

print( f' analytical expectation: {manual:g}\n' )


