## $ gnuplot -e "filename='f90.txt'" fitter.gnuplot

prefix = filename[0:strlen(filename)-4]

T1      = 0.0 + system("tail -n 1 " . filename . " | awk '{printf \"%.5f\", $2}'")
sigmaT1 = 0.0 + system("tail -n 1 " . filename . " | awk '{printf \"%.5f\", $3}'")

set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 12" linewidth 2
set output prefix.".png"
set size 0.8, 1.0

set title sprintf("%s: T_1 = %#.3f s,  {/Symbol s}_1 = %#.3g s", prefix, T1, sigmaT1)

P = 0.95
S(n) = 1.0 / ( 1.0 - P + P/n )

bars(val,std)  = sqrt( sigmaT1*sigmaT1/T1/T1 + std*std/val/val )

FIT_LIMIT = 1E-07
set fit quiet
set fit errorvariables
fit S(x) filename using 1:( T1/$2 ):( bars($2,$3) ) via P

stats filename using 1 nooutput
set key bottom right
set ylabel "Speed-up factor"
set xlabel "number of processes"
plot [0:STATS_max+1] [0:] filename using 1:( T1/$2 ):( (T1/$2)*bars($2,$3) ) \
 with yerrorbars title prefix, \
 S(x) with lines title sprintf("P_{fit} = %#.5f +/- %#.5f", P, P_err )

