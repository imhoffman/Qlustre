!! modified from http://people.sc.fsu.edu/~jburkardt/f_src/quad_mpi/quad_mpi.html
 module types
   use iso_fortran_env
   use mpi
   implicit none
   !! bit widths
   !! user must change MSG_SIZEs to match KINDs and recompile
   integer, parameter ::            rw = real64
   integer, parameter :: REAL_MSG_SIZE = MPI_REAL8
   integer, parameter ::            iw = int64
   integer, parameter ::  INT_MSG_SIZE = MPI_INTEGER8
   integer ( kind = iw ), parameter :: Ndesired = 1024*1024*1024*1_iw
 end module types

 !! subprograms
 module subs
   use types
   use omp_lib
   implicit none
   contains
   subroutine banner ( N, A, B, Nproc )
     implicit none
     integer (kind=iw), intent(in) :: N
     integer,           intent(in) :: Nproc
     real    (kind=rw), intent(in) :: A, B
     write ( 6, '(a)' ) ' '
     write ( 6, '(a)' ) '  Numerically integrate f(x) from A to B,'
     write ( 6, '(a)' ) ' '
     write ( 6, '(a,sp,f12.5,ss)' ) '  A        = ', A
     write ( 6, '(a,sp,f12.5,ss)' ) '  B        = ', B
     write ( 6, '(a,i12)' )   '  Ndesired = ', Ndesired
     write ( 6, '(a,i12)' )   '  Nactual  = ', N
     write ( 6, '(a)' )       ' '
     write ( 6, '(a)' )       '  using MPI to divide the computation'
     write ( 6, '(a,i0,a)' )  '  among ',Nproc-1,' parallel processes.'
    return
   end subroutine banner

   !! the function to be integerated
   pure function f ( x )
    implicit none
    real ( kind = rw )             :: f
    real ( kind = rw ), intent(in) :: x
    !f = x*x*x*x
    f = 4.0_rw * sqrt( 1.0_rw - x*x )
    return
   end function f

   !! Boole's Rule
   subroutine boole ( g, Npoints, xmin, xmax, area )
     implicit none
     integer (kind=iw), intent(in)    :: Npoints
     real    (kind=rw), intent(in)    :: xmin, xmax
     real    (kind=rw), intent(inout) :: area
     real    (kind=rw)                :: x, deltaX, h
     integer (kind=iw)                :: i
     interface
       pure function g ( x )
         use types
         real(kind=rw) :: g
         real(kind=rw), intent(in) :: x
       end function g
     end interface

     deltaX = (xmax-xmin)/real(Npoints,kind=rw)
     h = deltaX/4.0_rw
     !$OMP PARALLEL DO &
     !$OMP SHARED  ( deltaX, h, xmin, Npoints ) &
     !$OMP PRIVATE ( x, i ) &
     !$OMP DEFAULT ( none ) &
     !$OMP SCHEDULE( static ) &
     !$OMP REDUCTION ( +:area )
     do i = 1, Npoints
       x    = xmin + ( real(i-1,kind=rw) + 0.5_rw )*deltaX
       area = area + h*2.0_rw/45.0_rw * ( &
            &      7.0_rw*g( x - h*2.0_rw ) &
            &   + 32.0_rw*g( x - h )        &
            &   + 12.0_rw*g( x )            &
            &   + 32.0_rw*g( x + h )        &
            &   +  7.0_rw*g( x + h*2.0_rw ) &
            &       )
     end do
     !$OMP END PARALLEL DO
     return
   end subroutine boole

   !! Simpson's 3/8 Rule
   subroutine simpson ( g, Npoints, xmin, xmax, area )
     implicit none
     integer (kind=iw), intent(in)    :: Npoints
     real    (kind=rw), intent(in)    :: xmin, xmax
     real    (kind=rw), intent(inout) :: area
     real    (kind=rw)                :: x, deltaX
     integer (kind=iw)                :: i
     interface
       pure function g ( x )
         use types
         real(kind=rw) :: g
         real(kind=rw), intent(in) :: x
       end function g
     end interface

     deltaX = (xmax-xmin)/real(Npoints,kind=rw)
     !$OMP PARALLEL DO &
     !$OMP SHARED  ( deltaX, xmin, Npoints ) &
     !$OMP PRIVATE ( x, i ) &
     !$OMP DEFAULT ( none ) &
     !$OMP SCHEDULE( static ) &
     !$OMP REDUCTION ( +:area )
     do i = 1, Npoints
       x = xmin + ( real(i-1,kind=rw) + 0.5_rw )*deltaX
       area = area + deltaX/8.0_rw * ( &
            &           g( x - deltaX/2.0_rw ) &
            &  + 3.0_rw*g( x - deltaX/6.0_rw ) &
            &  + 3.0_rw*g( x + deltaX/6.0_rw ) &
            &         + g( x + deltaX/2.0_rw ) &
            &       )
     end do
     !$OMP END PARALLEL DO
     return
   end subroutine simpson

   !! the routine that came with the Burkardt code
   subroutine rect ( g, my_N, my_a, my_b, area )
     implicit none
     integer (kind=iw), intent(in)    :: my_N
     real    (kind=rw), intent(in)    :: my_a, my_b
     real    (kind=rw), intent(inout) :: area
     real    (kind=rw)                :: x
     integer (kind=iw)                :: i
     interface
       pure function g ( x )
         use types
         real(kind=rw) :: g
         real(kind=rw), intent(in) :: x
       end function g
     end interface

     !$OMP PARALLEL DO &
     !$OMP SHARED  ( my_N, my_a, my_b ) &
     !$OMP PRIVATE ( x, i ) &
     !$OMP DEFAULT ( none ) &
     !$OMP SCHEDULE( static ) &
     !$OMP REDUCTION ( +:area )
     do i = 1, my_N
       x = ( real ( my_N - i,     kind = rw ) * my_a   &
           + real (        i - 1, kind = rw ) * my_b ) &
           / real ( my_N     - 1, kind = rw )
       area = area + g ( x )
     end do
     !$OMP END PARALLEL DO

     area = ( my_b - my_a ) * area / real ( my_N, kind = rw )
     return
   end subroutine rect
 end module subs

 !!
 !! main program
 !!
 program main
   use iso_fortran_env
   use types
   use subs
   use mpi
   implicit none

   real    (kind= rw ) :: x, A, B, pA, pB
   real    (kind= rw ) :: subtotal=0.0_rw, total=0.0_rw, slice
   integer (kind= iw ) :: i, pN, N
   integer             :: p, Nproc, mpiid
   integer             :: head, sendr, recvr, tag, error_flag
   integer             :: msgstatus(MPI_STATUS_SIZE)
   real    (kind= 8 )  :: wtime

   A =  +0.0_rw
   !A =   -1.1_rw
   B =  +1.0_rw
   !B =   +0.713_rw

   head = 0

   call MPI_Init ( error_flag )
   call MPI_Comm_size ( MPI_COMM_WORLD, Nproc, error_flag )
   call MPI_Comm_rank ( MPI_COMM_WORLD, mpiid, error_flag )
   
   if ( mpiid == head ) then
     wtime = MPI_Wtime ( )

     !! integer arith ensures that N is whole multiple of pN
     pN = Ndesired / ( Nproc - 1_iw )
     N = ( Nproc - 1_iw ) * pN

     call banner ( N, A, B, Nproc )
  end if

  !! broadcast the number of points
  sendr = head
  call MPI_Bcast ( pN, 1, INT_MSG_SIZE, sendr, MPI_COMM_WORLD, error_flag )

  if ( mpiid == head ) then

    !! break up the domain for the different processes
    do p = 1, Nproc-1

      !! each MPI process is responsible for one slice of the domain
      slice = (B-A)/real(Nproc-1,kind=rw)
      pA    =  A  +  slice * real(p-1,kind=rw)
      pB    = pA  +  slice

      !! send out the assignments
      recvr = p
      tag   = 1
      call MPI_Send ( pA, 1, REAL_MSG_SIZE, recvr, &
              tag, MPI_COMM_WORLD, error_flag )
      if ( error_flag .ne. 0 ) goto 510
      tag   = 2
      call MPI_Send ( pB, 1, REAL_MSG_SIZE, recvr, &
              tag, MPI_COMM_WORLD, error_flag )
      if ( error_flag .ne. 0 ) goto 510
    end do
  else
    !! receive the assignments
    sendr = head
    tag   = 1
    call MPI_Recv ( pA, 1, REAL_MSG_SIZE, sendr, tag, &
      MPI_COMM_WORLD, msgstatus, error_flag )
    if ( error_flag .ne. 0 ) goto 510
    tag   = 2
    call MPI_Recv ( pB, 1, REAL_MSG_SIZE, sendr, tag, &
      MPI_COMM_WORLD, msgstatus, error_flag )
    if ( error_flag .ne. 0 ) goto 510

    !! compute the area
    !call simpson ( f, pN, pA, pB, subtotal )
    call boole ( f, pN, pA, pB, subtotal )
    !call rect ( f, pN, pA, pB, subtotal )
  end if

  !! collect and sum the subtotals
  call MPI_Reduce ( subtotal, total, 1, REAL_MSG_SIZE, &
    MPI_SUM, head, MPI_COMM_WORLD, error_flag )
  if ( error_flag .ne. 0 ) goto 510

  if ( mpiid == head ) then
    wtime = MPI_Wtime ( ) - wtime
    write(6,'(a)') ' '
    write(6,'(a,g32.24)') ' Analytical = ', 4.0_rw*atan(1.0_rw)
    !write(6,'(a,g32.24)') ' Analytical = ', (B*B*B*B*B-A*A*A*A*A)/5.0_rw
    write(6,'(a,g32.24)') '  Numerical = ', total
    write(6,'(a,g14.6)')  ' Total Time = ', wtime
  end if

  call MPI_Finalize ( error_flag )
  if ( mpiid .eq. head  .and.  error_flag .eq. 0 ) then
    write(6,'(a)') ' '
    write(6,'(a)') '  Normal end of execution.'
    write(6,'(a)') ' '
  end if

  500 goto 600
  510 write(6,*) ' bad error flag received'
  520 call exit(2)
  600 call exit(0)
 end program main
