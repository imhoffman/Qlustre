#include<stdio.h>
#include<math.h>
#include<mpi.h>
#include<omp.h>

// bit widths and MPI message sizes
//  !! suffixes must be added to all literals if changed !!
#define IW_t           long
#define INT_MSG_SIZE   MPI_LONG
#define RW_t           double
#define FLOAT_MSG_SIZE MPI_DOUBLE

// constants
IW_t   Ndesired = 1024*1024*1024*1L;

//
// sub programs
//
void
banner ( const double A, const double B, const IW_t N, const int Nproc )
{

 printf("\n");
 printf("  Numerically integrate f(x) from A to B,\n");
 printf("\n");
 printf("  A        = %+14.6f\n", A);
 printf("  B        = %+14.6f\n", B);
 printf("  Ndesired = %14ld\n", Ndesired);
 printf("  Nactual  = %14ld\n", N);
 printf("\n");
 printf("  using MPI to divide the computation\n");
 printf("  among %d parallel processes.", Nproc-1);

 return;
}

// the integrand
RW_t
f ( const RW_t x )
{
 //return x*x*x*x;
 return 4.0*sqrt( 1.0 - x*x );
}


void
boole ( RW_t (*g)(const RW_t),
	const IW_t Npoints, 
	const RW_t xmin, const RW_t xmax, 
	RW_t* area )
{
 RW_t x, deltaX, h;
 IW_t i;
 RW_t temp;
 deltaX =(RW_t) (xmax-xmin)/Npoints;
 h = deltaX/4.0;
 #pragma omp parallel for \
             shared  ( deltaX, h, g ) \
             private ( i, x ) \
             default ( none ) \
             schedule( static ) \
             reduction( +:temp )
             //shared  ( xmin, deltaX, h, Npoints, g )
 for ( i=1L; i<=Npoints; i++ ) {
   x     =(RW_t) xmin + ( i-1L + 0.5 )*deltaX;
   temp +=  + h*2.0/45.0 * ( 
             7.0*g(x - h*2.0) + 32.0*g(x - h) + 12.0*g(x)
           + 32.0*g(x + h) +  7.0*g(x + h*2.0));
 }
 *area = temp;
 return;
}


void
simpson ( RW_t (*g)(const RW_t), 
	  const IW_t Npoints, 
	  const RW_t xmin, 
	  const RW_t xmax, 
	  RW_t* area )
{
 RW_t x, deltaX;
 IW_t i;
 RW_t temp;
 deltaX =(RW_t) (xmax-xmin)/Npoints;
 #pragma omp parallel for \
             shared  ( deltaX, g ) \
             private ( i, x ) \
             default ( none ) \
             schedule( static ) \
             reduction( +:temp )
             //shared  ( xmin, deltaX, h, Npoints, g )
 for ( i=1L; i<=Npoints; i++ ) {
   x =(RW_t) xmin + ( i-1L + 0.5 )*deltaX;
   temp +=  deltaX/8.0 * ( 
		    g( x - deltaX/2.0 )
              + 3.0*g( x - deltaX/6.0 )
              + 3.0*g( x + deltaX/6.0 )
		  + g( x + deltaX/2.0 ) );
 }
 *area = temp;
 return;
}

//
// main program
// 
int
main ( int argc, char *argv[] )
{
  RW_t   x, A, B, pA, pB, subtotal=0.0, total=0.0, slice;
  double wtime;
  IW_t   i, pN, N;
  int    p, Nproc, mpiid, head, sendr, recvr, tag, err_flag=0;
  MPI_Status msgstatus;

  A =  +0.0;
  //A =  -1.100;
  B =  +1.0;
  //B =  +0.713;

  head = 0;

  err_flag += MPI_Init ( &argc, &argv );
  err_flag += MPI_Comm_size ( MPI_COMM_WORLD, &Nproc );
  err_flag += MPI_Comm_rank ( MPI_COMM_WORLD, &mpiid );
   
  if ( mpiid == head ) {
    wtime = MPI_Wtime();
    pN = Ndesired / ( Nproc - 1 );
    N  = ( Nproc - 1 ) * pN;
    banner ( A, B, N, Nproc );
  }

  // broadcast the number of points
  sendr = head;
  err_flag += MPI_Bcast ( &pN, 1, INT_MSG_SIZE, sendr, MPI_COMM_WORLD );

  if ( mpiid == head ) {

    for ( p=1; p<=Nproc-1; p++ ) {

      slice =(RW_t) (B-A)/(Nproc-1);
      pA    =(RW_t)  A  +  slice * (p-1);
      pB    =       pA  +  slice;

      // send out the assignments
      recvr = p;
      tag = 1;
      err_flag += MPI_Send ( &pA, 1,
		      FLOAT_MSG_SIZE, recvr, tag, MPI_COMM_WORLD );
      tag = 2;
      err_flag += MPI_Send ( &pB, 1,
		      FLOAT_MSG_SIZE, recvr, tag, MPI_COMM_WORLD );
    }
  } else {
    // receive the assignments
    sendr = head;
    tag = 1;
    err_flag += MPI_Recv ( &pA, 1, 
		    FLOAT_MSG_SIZE, sendr, tag, MPI_COMM_WORLD, &msgstatus );
    tag = 2;
    err_flag += MPI_Recv ( &pB, 1,
		    FLOAT_MSG_SIZE, sendr, tag, MPI_COMM_WORLD, &msgstatus );

    // compute the area
    subtotal = 0.0;
    //simpson ( &f, pN, pA, pB, &subtotal );
    boole ( &f, pN, pA, pB, &subtotal );
  }

  // collect and sum the subtotals
  err_flag += MPI_Reduce ( &subtotal, &total, 1,
		  FLOAT_MSG_SIZE, MPI_SUM, head, MPI_COMM_WORLD );

  if ( mpiid == head ) {
    wtime = MPI_Wtime() - wtime;
    printf("\n\n");
    printf(" Analytical = %32.24f\n", 4.0*atan(1.0) );
    //printf(" Analytical = %32.24f\n", (B*B*B*B*B-A*A*A*A*A)/5.0 );
    printf("  Numerical = %32.24f\n", total );
    printf(" Total Time = %14.6g\n",  wtime );
  }

  err_flag += MPI_Finalize();
  if ( mpiid == head && err_flag == 0 ) {
    printf("\n  Normal end of execution.\n\n");
  }

  return 0;
}

