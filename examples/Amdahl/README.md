## Amdahl's Law

I've rewritten [the Fortran version](nq-mpi-omp.f90) and [the C version](nq-mpi-omp.c) of [my quadrature code](https://gitlab.com/imhoffman/Qlustre/-/tree/master/examples/quadrature):
1. to divide the points equally among the MPI processes, and
1. to use OpenMP among local cores and to use MPI among network nodes.

The repo contains speedup data when varying number of MPI processes and OMP threads with command such as
```
$ mpiexec -n 8 -env OMP_NUM_THREADS 4 -f hostfile-omp ./a.out
```

The data are then fitted [in gnuplot for parallelized fraction *P*](fitter.gnuplot).
The absolute time of the C programs are much longer than the Fortran, so I must have made some poor decisions with the C code---nevertheless, it scales properly with resources.

Here is a summary of the fitted parallel fractions *P*.

| Language | Variation | P |
| :--      | :--       | :-- |
| C        | MPI       | 0.9987(2)|
| Fortran  | MPI       | 0.9983(2)|
| C        | OMP       | 0.9980(2)|
| Fortran  | OMP       | 0.9974(1)|

![f90-varmpi-omp.png](f90-varmpi-omp.png)
![f90-mpi-varomp.png](f90-mpi-varomp.png)

![c-varmpi-omp.png](c-varmpi-omp.png)
![c-mpi-varomp.png](c-mpi-varomp.png)

